from hellish import *
from build_user import *
import shlex

print("# auto-generated build file") > "build.ninja"
print(f"includes = {include_flags}") >> "build.ninja"
print("include rules.ninja") >> "build.ninja"

entry_point_test2 = "test2/test2.cpp"

files = glob("test2/*.cpp")
E.ONLY_TEST2 @= ""
if not E.ONLY_TEST2:
    game_files = glob("game/**/*.cpp") + glob("game/*.cpp")
    if not game_files:
        print("could not find any game files, building as if with $ONLY_TEST2")
    else:
        if entry_point_test2 in files:
            files.remove(entry_point_test2)
        files += game_files

print("generating", *files)

output_files = []
for file in files:
    output_files += [f"build/{file}.o"]
    print(f"build build/{file}.o: cpp {file}") >> "build.ninja"

print() >> "build.ninja"

shader_files = glob("shaders/*.glsl")
shader_output_files = []
for file in shader_files:
    shader_output_files += [f"{file}.spv"]
    print(f"build {file}.spv: glslc {file}") >> "build.ninja"
    if "vert" in file: print("  stage = vert") >> "build.ninja"
    if "frag" in file: print("  stage = frag") >> "build.ninja"

print() >> "build.ninja"

print(f"build main: link {' '.join(output_files)} | {' '.join(shader_output_files)}") >> "build.ninja"
print(f"  ldflags = {ldflags}\n") >> "build.ninja"
