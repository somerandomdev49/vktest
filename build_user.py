from hellish import *
import sys
from os.path import *

E.VULKAN_SDK_PATH @= "D:/VulkanSDK/1.2.198.1" # My Setup!

if "darwin" in sys.platform:
	include_flags = f"-I{abspath(curdir)}"
	ldflags = "-framework IOKit -framework Cocoa -lvulkan -lglfw3 -Wl,-w"
elif "cygwin" in sys.platform or "win" in sys.platform:
	include_flags = f"-I{E.VULKAN_SDK_PATH}/Include -I{abspath(curdir)}"
	ldflags = f"-L{E.VULKAN_SDK_PATH}/Lib -l:vulkan-1.lib -lglfw3"
elif "linux" in sys.platform: # untested but will test soon
	include_flags = f"-I{abspath(curdir)}"
	ldflags = "-lvulkan-1 -lglfw"
elif "bsd" in sys.platform: # untested
	include_flags = f"-I{abspath(curdir)}"
	ldflags = "-lvulkan-1 -lglfw"
else:
	print(f"Unknown platform: {sys.platform}") >> stderr
	exit(1)
