#include "common.hpp"
#include "engine.hpp"
#include "swapchain.hpp"
#include "platform.hpp"
#include "pipeline.hpp"
#include "renderpass.hpp"
#include "shader.hpp"
#include "vertex.hpp"
#include "descriptors.hpp"
#include "vulkan/vulkan_core.h"
#include <stdexcept>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <stdint.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <entt/entity/registry.hpp>

static std::vector<char> readFile(const std::string &filename) {
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    CHECK(file.is_open(), "Failed to open file: '" + std::string(filename) + "'.");

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);
	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();
	return buffer;
}

t2::ShaderModule createShaderModule(t2::Engine &engine, const char *entry, const char *path) {
	t2::ShaderModule module(engine, entry);
	auto data = readFile(path);
	module.initialize(data.size(), (uint32_t*)data.data());
	return module;
}

struct Vertex {
	using Info = t2::VertexBase<glm::vec3, glm::vec3, glm::vec2>;
	glm::vec3 position;
	glm::vec3 color;
	glm::vec2 texcoord;
};

void transitionImageLayout(t2::CommandBuffer &commandBuffer, t2::Image &image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout) {
	VkPipelineStageFlags srcStage;
	VkPipelineStageFlags dstStage;

	t2::ImageMemoryBarrier barrier { .image = image };
	if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	} else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	} else {
		throw std::invalid_argument("Unsupported layout transition");
	}

	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.range = {
		.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		.baseMipLevel = 0,
		.levelCount = 1,
		.baseArrayLayer = 0,
		.layerCount = 1,
	};

	commandBuffer.cmdPipelineBarrier(srcStage, dstStage, barrier);
}

void transferImageData(t2::Engine &engine, t2::CommandPool &commandPool, t2::Image &image, u::span<const uint8_t> data) {
	auto stagingBuffer = image.allocator->createBuffer(
		data.size(),
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		t2::ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | t2::ALLOCATION_CREATE_MAPPED_BIT
	);

	image.allocator->writeBuffer(stagingBuffer, data);

	t2::CommandBuffer commandBuffer(engine);
	commandBuffer.initialize(commandPool);
	commandBuffer.begin();
	transitionImageLayout(
		commandBuffer,
		image,
		VK_FORMAT_R8G8B8A8_SRGB,
		image.layout,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
	);

	VkBufferImageCopy copy{};
	copy.bufferOffset = 0;
	copy.bufferRowLength = 0;
	copy.bufferImageHeight = 0;

	copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copy.imageSubresource.mipLevel = 0;
	copy.imageSubresource.baseArrayLayer = 0;
	copy.imageSubresource.layerCount = 1;

	copy.imageOffset = {0, 0, 0};
	copy.imageExtent = {
		image.extent.width,
		image.extent.height,
		1
	};

	commandBuffer.cmdCopyBufferToImage(stagingBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, { 1, &copy });
	transitionImageLayout(
		commandBuffer,
		image,
		VK_FORMAT_R8G8B8A8_SRGB,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
	);
	commandBuffer.end();

	engine.queues.graphics.submitCommandBuffer(commandBuffer);
	engine.queues.graphics.waitIdle();

	commandPool.freeCommandBuffer(commandBuffer);
	stagingBuffer.allocator->destroyBuffer(stagingBuffer);
}

void transferBufferData(t2::Engine &engine, t2::CommandPool &commandPool, t2::Buffer &targetBuffer, u::span<const uint8_t> data) {
	auto stagingBuffer = targetBuffer.allocator->createBuffer(
		data.size(),
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		t2::ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | t2::ALLOCATION_CREATE_MAPPED_BIT
	);

	targetBuffer.allocator->writeBuffer(stagingBuffer, data);
	VkBufferCopy copy { 0, 0, targetBuffer.size };

	t2::CommandBuffer commandBuffer(engine);
	commandBuffer.initialize(commandPool);
	commandBuffer.begin();
	commandBuffer.cmdCopyBuffer(stagingBuffer, targetBuffer, { 1, &copy });
	commandBuffer.end();

	engine.queues.graphics.submitCommandBuffer(commandBuffer);
	engine.queues.graphics.waitIdle();

	commandPool.freeCommandBuffer(commandBuffer);
	stagingBuffer.allocator->destroyBuffer(stagingBuffer);
}

template<typename T>
void transferBufferData(t2::Engine &engine, t2::CommandPool &commandPool, t2::Buffer &targetBuffer, u::span<const T> data) {
	transferBufferData(engine, commandPool, targetBuffer, { data.size() * sizeof(T), (uint8_t*)data.data() });
}

template<typename T>
struct MaybeUninit {
	uint8_t bytes[sizeof(T)];
	
	MaybeUninit() {}
	~MaybeUninit() { get().~T(); }

	template<typename ...Args>
	MaybeUninit(Args &&...args) {
		initialize(std::forward<Args>(args)...);
	}

	template<typename ...Args>
	T &initialize(Args &&...args) {
		new((T*)this->bytes) T(std::forward<Args>(args)...);
		return get();
	}

	operator const T&() const { return get(); }
	operator T&() { return get(); }
	
	const T &get() const { return *(T*)this->bytes; }
	T &get() { return *(T*)this->bytes; }
};

struct Texture {
	MaybeUninit<t2::DescriptorSet> set;
	MaybeUninit<t2::Image> image;
	t2::ImageView view;

	Texture(t2::Engine &engine)
		: view(engine, *(t2::Image*)(&this->image)) {}
};

struct CreateTextureParams {
	t2::Engine &engine;
	t2::Allocator &allocator;
	t2::CommandPool &commandPool;
	t2::ImageSampler &imageSampler;
	t2::DescriptorPool &descriptorPool;
	t2::DescriptorSetLayout &descriptorSetLayout;
};

void createTexture(std::vector<Texture*> &textures, const char *path, const CreateTextureParams &params) {
	int imageWidth, imageHeight, imageChannels;
	uint8_t *imageData = stbi_load(path, &imageWidth, &imageHeight, &imageChannels, STBI_rgb_alpha);

	textures.push_back(new Texture(params.engine));
	Texture *out = textures[textures.size() - 1];
	out->image.get() = std::move(params.allocator.createImage(t2::ImageInfo {
		.type = t2::IMAGE_TYPE_2D,
		.extent = { .width = (uint32_t)imageWidth, .height = (uint32_t)imageHeight, .depth = 1 },
		.mipLevels = 1,
		.arrayLayers = 1,
		.format = VK_FORMAT_R8G8B8A8_SRGB,
		.tiling = t2::IMAGE_TILING_OPTIMAL,
		.isPreinit = false,
		.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.samples = VK_SAMPLE_COUNT_1_BIT,
	}));

	out->view.initialize(VK_IMAGE_VIEW_TYPE_2D, VK_FORMAT_R8G8B8A8_SRGB, {
		.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		.baseMipLevel = 0,
		.levelCount = 1,
		.baseArrayLayer = 0,
		.layerCount = 1,
	});

	transferImageData(params.commandPool.engine, params.commandPool, out->image.get(), {
		size_t(imageWidth * imageHeight) * 4,
		imageData
	});

	out->set = params.descriptorPool.allocate(params.descriptorSetLayout);
	out->set.get().write(0, t2::DescriptorImageInfo {
		.view = out->view,
		.sampler = params.imageSampler,
		.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
	});

	free(imageData);
}

struct TransformComponent {
	glm::vec2 position;
	float rotation;
	glm::vec2 scale;
};

struct GraphicsComponent {
	size_t texture;
};

class Game {
public:
	int run(void *context, void (*initialize)(entt::registry&, void*, t2::Engine&), void (*update)(entt::registry&, void*, float)) {
		if(!glfwInit()) {
			const char *errorString;
			glfwGetError(&errorString);
			DEBUG_ERROR("Failed to initialize GLFW. Error: " << errorString);
			return 1;
		}

		DEBUG("Initialized GLFW");

		t2::AppInfo appInfo { "test app", VK_MAKE_VERSION(1, 0, 0), 800, 600 };
		t2::Engine engine(appInfo);
		t2::Swapchain swapchain(engine);
		t2::Pipeline pipeline(engine);
		t2::RenderPass renderPass(engine);
		t2::Shader shader(engine);
		t2::SyncObjects sync(engine);
		t2::CommandPool commandPool(engine);
		t2::CommandBuffer commandBuffer(engine);
		t2::Allocator allocator(engine);
		t2::DescriptorPool descriptorPool(engine);

		t2::DescriptorSetLayout setLayouts[] = {
			t2::DescriptorSetLayout(engine), // texture
			t2::DescriptorSetLayout(engine), // entity
		};

		t2::DescriptorSetLayoutBinding setLayoutBindings[2][2] = {
			{ { t2::DESCRIPTOR_COMBINED_IMAGE_SAMPLER, 0, 1, t2::SHADER_FRAGMENT_BIT }, },
			{ { t2::DESCRIPTOR_UNIFORM_BUFFER_DYNAMIC, 0, 1, t2::SHADER_VERTEX_BIT }, }
		};

		t2::DescriptorPoolSizeForType poolSizes[] = {
			{ t2::DESCRIPTOR_COMBINED_IMAGE_SAMPLER, 9 },
			{ t2::DESCRIPTOR_UNIFORM_BUFFER_DYNAMIC, 1 },
		};

		auto vertexAttributeDescriptions = Vertex::Info::getAttributeDescriptions();
		auto vertexBindingDescription = Vertex::Info::getBindingDescription();

		const t2::DescriptorSetLayout *setLayoutPtrs[] = { &setLayouts[0], &setLayouts[1] };
		t2::PipelineInfo pipelineInfo;
		pipelineInfo.layoutInfo.descriptorSetLayouts = { 2, &setLayoutPtrs[0] };
		pipelineInfo.colorBlendingInfo.enabled = false;
		pipelineInfo.rasterizerInfo.enableDiscard = false;
		pipelineInfo.rasterizerInfo.cullMode = t2::CULL_MODE_NONE;
		pipelineInfo.rasterizerInfo.polygonMode = t2::POLYGON_MODE_FILL;
		pipelineInfo.rasterizerInfo.lineWidth = 1.0f;
		pipelineInfo.scissorInfo.offset = { 0, 0 };
		pipelineInfo.scissorInfo.extent = { 0, 0 };
		pipelineInfo.viewportExtent     = { 0, 0 };
		pipelineInfo.vertexInputInfo.bindingDescriptions = { 1, &vertexBindingDescription };
		pipelineInfo.vertexInputInfo.attributeDescriptions = {
			vertexAttributeDescriptions.size(),
			vertexAttributeDescriptions.data()
		};

		try {
			DEBUG("initializing...");
			engine.initialize();
			allocator.initialize();
			swapchain.initialize();
			
			setLayouts[0].initalize({ 1, setLayoutBindings[0] });
			setLayouts[1].initalize({ 1, setLayoutBindings[1] });
			descriptorPool.initialize(10, { 2, poolSizes });

			shader.add(t2::SHADER_TYPE_VERTEX, createShaderModule(engine, "main", "shaders/vert.glsl.spv"));
			shader.add(t2::SHADER_TYPE_FRAGMENT, createShaderModule(engine, "main", "shaders/frag.glsl.spv"));

			VkAttachmentDescription colorAttachment
			{
				.format = swapchain.surfaceFormat.format,
				.samples = VK_SAMPLE_COUNT_1_BIT,
				.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
				.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
				.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
				.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			};

			VkAttachmentDescription depthAttachment
			{
				.format = swapchain.surfaceFormat.format,
				.samples = VK_SAMPLE_COUNT_1_BIT,
				.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
				.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
				.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
				.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			};

			t2::Subpass subpass;
			{
				subpass.bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

				subpass.dependency.enabled = false;
				subpass.dependency.subpass = VK_SUBPASS_EXTERNAL;
				subpass.dependency.dependencyFlags = 0;
				subpass.dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
				subpass.dependency.srcAccessMask = 0;
				subpass.dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
				subpass.dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

				subpass.references.push_back(t2::AttachmentReference {
					/* type */ t2::SUBPASS_ATTACHMENT_TYPE_COLOR,
					/* attachment */ 0,
					/* layout */ VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
				});

				// subpass.references.push_back(t2::AttachmentReference {
				// 	.type = t2::SUBPASS_ATTACHMENT_TYPE_DEPTH_STENCIL,
				// 	.attachment = 1,
				// 	.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
				// });
			}

			renderPass.initialize({ colorAttachment /* , depthAttachment */ }, { subpass });
			swapchain.initializeFramebuffers(renderPass);

			pipelineInfo.scissorInfo.extent = swapchain.extent;
			pipelineInfo.viewportExtent = swapchain.extent;
			pipeline.initialize(pipelineInfo, shader, renderPass);

			sync.initialize();
			commandPool.initialize(engine.physicalDevice.queueFamilies.graphics);
			commandBuffer.initialize(commandPool);

			const std::vector<Vertex> vertices = {
				{ { -0.5f, -0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } },
				{ {  0.5f, -0.5f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
				{ {  0.5f,  0.5f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f } },
				{ { -0.5f,  0.5f, 0.0f }, { 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
			};

			const std::vector<uint16_t> indices = {
				0, 1, 2, 2, 3, 0
			};

			auto vertexBuffer = allocator.createBuffer(
				vertices.size() * sizeof(decltype(vertices)::value_type),
				VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
			);

			auto indexBuffer = allocator.createBuffer(
				indices.size() * sizeof(decltype(indices)::value_type),
				VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
			);
			

			t2::ImageSampler imageSampler(engine);
			imageSampler.initialize(t2::ImageSamplerInfo{
				.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
				.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
				.filter = {
					.mag = VK_FILTER_LINEAR,
					.min = VK_FILTER_LINEAR,
				},
				.addressMode = {
					.u = VK_SAMPLER_ADDRESS_MODE_REPEAT,
					.v = VK_SAMPLER_ADDRESS_MODE_REPEAT,
					.w = VK_SAMPLER_ADDRESS_MODE_REPEAT,
				},
				.anisotropy = {
					.enable = false,
					.max = 1.0f
				},
				.compare = {
					.enable = false,
					.op = VK_COMPARE_OP_ALWAYS
				},
				.lod = {
					.bias = 0.0f,
					.min = 0.0f,
					.max = 0.0f
				}
			});

			struct UniformBuffer {
				glm::mat4 transform;
			};

			auto minUboAlignment = engine.physicalDevice.properties.limits.minUniformBufferOffsetAlignment;
			auto uniformBufferSize = sizeof(UniformBuffer);
			if(minUboAlignment > 0)
				uniformBufferSize = (uniformBufferSize + minUboAlignment - 1) & ~(minUboAlignment - 1);
			DEBUG("minUboAlignment   = " << minUboAlignment);
			DEBUG("uniformBufferSize = " << uniformBufferSize);

			std::vector<Texture*> textures { };

			{
				CreateTextureParams params {
					.engine = engine,
					.allocator = allocator,
					.commandPool = commandPool,
					.imageSampler = imageSampler,
					.descriptorPool = descriptorPool,
					.descriptorSetLayout = setLayouts[0]
				};

				createTexture(textures, "img/o.png", params);
				createTexture(textures, "img/dragonbook.png", params);
				createTexture(textures, "img/birb.png", params);
				createTexture(textures, "img/projectile.png", params);
			}

			t2::DescriptorSet uniformDescriptor = { descriptorPool.allocate(setLayouts[1]) };
#define MAX_QUADS 128
			uint8_t *uniformData = new uint8_t[uniformBufferSize * MAX_QUADS];

			auto uniformBuffer = allocator.createBuffer(
				uniformBufferSize * MAX_QUADS,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				t2::ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT
			);

			uniformDescriptor.write(0, t2::DescriptorBufferInfo{
				.buffer = uniformBuffer,
				.offset = 0,
				.size = sizeof(UniformBuffer)
			});
			
			transferBufferData<const Vertex>(engine, commandPool, vertexBuffer, { vertices.size(), vertices.data() });
			transferBufferData<const uint16_t>(engine, commandPool, indexBuffer, { indices.size(), indices.data() });

			entt::registry registry;
			initialize(registry, context, engine);

			DEBUG("starting...");

			size_t frameCounter = 0;
			float totalTime = 0;
			float lastTime = engine.getTime();
			while(!engine.shouldEnd()) {
				float currentTime = engine.getTime();
				float deltaTime = currentTime - lastTime;
				totalTime += deltaTime;
				frameCounter += 1;

				sync.waitFrame();

				update(registry, context, deltaTime);
				size_t renderableCount = 0;
				glm::ivec2 framebufferSize;
				glfwGetFramebufferSize(engine.window, &framebufferSize.x, &framebufferSize.y);
				registry.view<TransformComponent, GraphicsComponent>().each([&](auto &transform, auto &graphics) {
					uint32_t offset = renderableCount * uniformBufferSize;
					UniformBuffer *buffer = (UniformBuffer*)(uniformData + offset);

					auto projection = glm::ortho(0.0f, (float)framebufferSize.x, 0.0f, (float)framebufferSize.y);

					buffer->transform = glm::mat4(1.0f);
					buffer->transform = glm::translate(buffer->transform, glm::vec3(transform.position, 0));
					buffer->transform = glm::rotate(buffer->transform, transform.rotation, glm::vec3(0, 0, 1));
					buffer->transform = glm::scale(buffer->transform, glm::vec3(transform.scale, 0));
					buffer->transform = projection * buffer->transform;

					renderableCount += 1;
				});

				uniformBuffer.write<uint8_t>({ uniformBufferSize * (renderableCount + 1), uniformData });
				
				uint32_t nextImage = swapchain.acquireNextImage(sync);
				commandBuffer.reset();
				commandBuffer.begin();
				commandBuffer.cmdBeginRenderPass(swapchain.framebuffers[nextImage], {{ 0, 0 }, swapchain.extent}, {{{0.0f, 0.0f, 0.0f, 1.0f}}});
				commandBuffer.cmdBindPipeline(pipeline);
				commandBuffer.cmdBindIndexBuffer({ indexBuffer, 0 }, VK_INDEX_TYPE_UINT16);
				commandBuffer.cmdBindVertexBuffer({ vertexBuffer, 0 });
				{
					size_t lastTexture = 0;
					const t2::DescriptorSet *set = &textures[0]->set.get();
					commandBuffer.cmdBindDescriptorSets(pipeline, 0, { 1, &set });
					size_t i = 0;
					registry.view<TransformComponent, GraphicsComponent>().each([&](auto &transform, auto &graphics) {
						if(graphics.texture != lastTexture) {
							const t2::DescriptorSet *set = &textures[graphics.texture]->set.get();
							commandBuffer.cmdBindDescriptorSets(pipeline, 0, { 1, &set });
						}
						uint32_t offset = i * uniformBufferSize;
						const t2::DescriptorSet *set = &uniformDescriptor;
						commandBuffer.cmdBindDescriptorSets(pipeline, 1, { 1, &set }, { 1, &offset });
						commandBuffer.cmdDrawIndexed(indices.size());
						i += 1;
					});
				}
				commandBuffer.cmdEndRenderPass();
				commandBuffer.end();
				
				engine.queues.graphics.submitCommandBuffer(commandBuffer, sync);
				engine.queues.present.present(swapchain, nextImage, sync);

				engine.frame();

				if(totalTime >= 0.1f) {
					float fps = frameCounter / totalTime;
					engine.setTitle("FPS: " + std::to_string((int)fps));
					totalTime = 0.0f;
					frameCounter = 0;
				}

				lastTime = currentTime;
			}

			vkDeviceWaitIdle(engine.device);

			DEBUG("deinitializing...");
			imageSampler.deinitialize();
			delete[] uniformData;
			for(auto &texture : textures) {
				texture->view.deinitialize();
				allocator.destroyImage(texture->image);
				delete texture;
			}
			allocator.destroyBuffer(vertexBuffer);
			allocator.destroyBuffer(indexBuffer);
			allocator.destroyBuffer(uniformBuffer);
			for(auto &layout : setLayouts)
				layout.deinitialize();
			descriptorPool.deinitialize();
			commandPool.deinitialize();
			sync.deinitialize();
			pipeline.deinitialize();
			renderPass.deinitialize();
			shader.deinitialize();
			swapchain.deinitialize();
			allocator.deinitialize();
			engine.deinitialize();
		} catch(std::runtime_error &e) {
			DEBUG_ERROR(e.what());
			return 1;
		}

		DEBUG("Done");
		glfwTerminate();
		return 0;
	}
};

struct PlayerComponent { float speed; };
struct ProjectileComponent { glm::vec2 delta; };

int main() {
	platform_prep(); // sets useful stuff for each platform (e.g. allows ANSI escapes on Windows)
	void *ptr;
	return Game().run(&ptr, [](entt::registry &registry, void *context, t2::Engine &engine) {
		*(t2::Engine**)context = &engine;

		auto a = registry.create();
		registry.emplace<TransformComponent>(a, glm::vec3{0.0f, 0.0f, 0.0f}, 0.0f, glm::vec2{64.0f, 64.0f});
		registry.emplace<GraphicsComponent>(a, 0);
		registry.emplace<PlayerComponent>(a, 100.0f);
	}, [](entt::registry &registry, void *context, float deltaTime) {
		t2::Engine &engine = **(t2::Engine**)context;
		

		registry.view<TransformComponent, PlayerComponent>().each([&](TransformComponent &transform, PlayerComponent &player) {
			if(glfwGetKey(engine.window, GLFW_KEY_A)) transform.position.x -= player.speed * deltaTime;
			if(glfwGetKey(engine.window, GLFW_KEY_D)) transform.position.x += player.speed * deltaTime;
			if(glfwGetKey(engine.window, GLFW_KEY_W)) transform.position.y -= player.speed * deltaTime;
			if(glfwGetKey(engine.window, GLFW_KEY_S)) transform.position.y += player.speed * deltaTime;
			if(glfwGetKey(engine.window, GLFW_KEY_SPACE)) {
				double xpos, ypos;
				glfwGetCursorPos(engine.window, &xpos, &ypos);
				float angle = atan2f(transform.position.y - ypos, xpos - transform.position.x);
				glm::vec2 delta = { cosf(angle) * 5.0f, sinf(angle) * 5.0f };
				transform.rotation = angle;
				// auto e = registry.create();
				// registry.emplace<TransformComponent>(e, transform.position, angle, glm::vec2(32.0f, 32.0f));
				// registry.emplace<ProjectileComponent>(e, delta);
				// registry.emplace<GraphicsComponent>(e, 3);
			}
		});

		registry.view<TransformComponent, ProjectileComponent>().each([&](TransformComponent &transform, ProjectileComponent &projectile) {
			transform.position += projectile.delta * deltaTime;
		});
	});
}
