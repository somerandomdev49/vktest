#include "engine.hpp"
#include "common.hpp"
#include "vulkan/vulkan_core.h"
#include <algorithm>
#include <cstring>
#include <set>

#define VALIDATION_LAYERS_ENABLED 1

#if VALIDATION_LAYERS_ENABLED
VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallback(
    VkDebugReportFlagsEXT       flags,
    VkDebugReportObjectTypeEXT  objectType,
    uint64_t                    object,
    size_t                      location,
    int32_t                     messageCode,
    const char*                 pLayerPrefix,
    const char*                 pMessage,
    void*                       pUserData)
{
    DEBUG_ERROR(pMessage);
    return VK_FALSE;
}
#endif

namespace t2 {
	void Engine::initialize() {
		DEBUG_FUNC();
		this->initializeWindow();
		this->initializeInstance();
		this->initializeSurface();
		this->initializeLogicalDevice();
	}

	void Engine::initializeLogicalDevice() {
		auto requiredDeviceExtensions = this->selectPhysicalDevice();

		DEBUG_FUNC();
		float queuePriority = 1.0f;
		
		const auto createQueueCreateInfo = [&queuePriority](uint32_t index, float *priority = nullptr) -> VkDeviceQueueCreateInfo {
			VkDeviceQueueCreateInfo createInfo {};
			createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			createInfo.queueFamilyIndex = index;
			createInfo.queueCount = 1;
			createInfo.pQueuePriorities = priority ? priority : &queuePriority;
			return createInfo;
		};
		
		std::set<uint32_t> queueIndices = {
			this->physicalDevice.queueFamilies.graphics,
			this->physicalDevice.queueFamilies.present
		};

		std::vector<VkDeviceQueueCreateInfo> queueInfos(queueIndices.size());
		std::transform(queueIndices.begin(), queueIndices.end(), queueInfos.begin(), createQueueCreateInfo);

		VkPhysicalDeviceFeatures deviceFeatures {};
		(void)deviceFeatures;

		requiredDeviceExtensions.insert(
			requiredDeviceExtensions.end(),
			PhysicalDevice::requiredDeviceExtensions.begin(),
			PhysicalDevice::requiredDeviceExtensions.end()
		);

		VkDeviceCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		createInfo.queueCreateInfoCount = (uint32_t)queueInfos.size();
		createInfo.pQueueCreateInfos = queueInfos.data();
		createInfo.pEnabledFeatures = &deviceFeatures;
		createInfo.enabledExtensionCount = (uint32_t)requiredDeviceExtensions.size();
		createInfo.ppEnabledExtensionNames = requiredDeviceExtensions.data();
		createInfo.enabledLayerCount = 0;
		createInfo.ppEnabledLayerNames = nullptr;

		CHECKVK(
			vkCreateDevice(this->physicalDevice.device, &createInfo, nullptr, &this->device),
			"Failed to create logical device"
		);

		deinit([this]() {
			vkDestroyDevice(this->device, nullptr);
		});

		this->queues.graphics.engine = this;
		this->queues.present.engine = this;

		vkGetDeviceQueue(this->device, this->physicalDevice.queueFamilies.graphics, 0, &this->queues.graphics.queue);
		vkGetDeviceQueue(this->device, this->physicalDevice.queueFamilies.present, 0, &this->queues.present.queue);
	}
	
	std::vector<const char*> Engine::selectPhysicalDevice() {
		DEBUG_FUNC();
		uint32_t deviceCount;
		vkEnumeratePhysicalDevices(this->instance, &deviceCount, nullptr);

		std::vector<VkPhysicalDevice> devices(deviceCount);
		vkEnumeratePhysicalDevices(this->instance, &deviceCount, devices.data());

		std::vector<const char*> requiredDeviceExtensions;
		
		// iterate to find a suitable device.
		bool found = false;
		for(VkPhysicalDevice device : devices) {
			PhysicalDevice physicalDevice;
			physicalDevice.device = device;
		
			// get the properties
			vkGetPhysicalDeviceProperties(device, &physicalDevice.properties);

			// get queue families
			uint32_t queueFamilyCount = 0;
			vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

			std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
			vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

			// set the corresponding indices
			uint32_t index = 0;
			for(VkQueueFamilyProperties queueFamilyProperties : queueFamilies) {
				if(queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT)
					physicalDevice.queueFamilies.graphics = index;

				if(queueFamilyProperties.queueFlags & VK_QUEUE_TRANSFER_BIT)
					physicalDevice.queueFamilies.transfer = index;
				
				VkBool32 presentSupport = false;
				vkGetPhysicalDeviceSurfaceSupportKHR(device, index, surface, &presentSupport);
				if(presentSupport) physicalDevice.queueFamilies.present = index;

				index += 1;
			}

			vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice.device, this->surface, &physicalDevice.surfaceSupport.capabilities);

			uint32_t formatCount;
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

			if(formatCount != 0) {
				physicalDevice.surfaceSupport.formats.resize(formatCount);
				vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, physicalDevice.surfaceSupport.formats.data());
			}
			
			uint32_t presentModeCount;
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

			if(presentModeCount != 0) {
				physicalDevice.surfaceSupport.presentModes.resize(presentModeCount);
				vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, physicalDevice.surfaceSupport.presentModes.data());
			}
			
			std::vector<const char*> suggestedDeviceExtensions = {
				"VK_KHR_portability_subset"
			};

			uint32_t extensionCount;
			vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
			physicalDevice.availableExtensions.resize(extensionCount);
			vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, physicalDevice.availableExtensions.data());

			for(const auto &properties : physicalDevice.availableExtensions) {
				for(auto it = suggestedDeviceExtensions.begin(); it != suggestedDeviceExtensions.end(); ++it) {
					const char *suggested = *it;
					if(strcmp(properties.extensionName, suggested) == 0) {
						requiredDeviceExtensions.push_back(suggested);
						suggestedDeviceExtensions.erase(it);
						break;
					}
				}
			}

			if(physicalDevice.suitable()) {
				std::swap(this->physicalDevice, physicalDevice);
				found = true;
				break;
			}
		}

		CHECK(found, "Failed to find a suitable GPU!");
		return requiredDeviceExtensions;
	}

	void Engine::initializeSurface() {
		DEBUG_FUNC();
		CHECKVK(glfwCreateWindowSurface(this->instance, this->window, nullptr, &this->surface), "Could not create window surface");
		deinit([this]() {
			vkDestroySurfaceKHR(this->instance, this->surface, nullptr);
		});
	}

	void Engine::initializeInstance() {
		DEBUG_FUNC();
		VkApplicationInfo appInfo {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.apiVersion = VK_API_VERSION_1_0;
		appInfo.engineVersion = VK_MAKE_VERSION(0, 1, 0);
		appInfo.pEngineName = "Test2CPP";
		appInfo.pApplicationName = this->appInfo.name.data();
		appInfo.applicationVersion = this->appInfo.version;

		std::vector<const char*> suggestedInstanceExtensions = {
			"VK_EXT_debug_report",
			"VK_KHR_get_physical_device_properties2"
		};

		std::vector<const char*> suggestedInstanceLayers = {
			"VK_LAYER_KHRONOS_validation"
		};

		std::vector<const char*> requiredInstanceExtensions;
		{
			uint32_t extensionCount;
			glfwGetRequiredInstanceExtensions(&extensionCount);
			requiredInstanceExtensions.resize(extensionCount);

			auto extensions = glfwGetRequiredInstanceExtensions(&extensionCount);
			for(size_t i = 0; i < extensionCount; ++i) {
				requiredInstanceExtensions[i] = extensions[i];
			}

			uint32_t propertyCount = 0;
			vkEnumerateInstanceExtensionProperties(nullptr, &propertyCount, nullptr);
			std::vector<VkExtensionProperties> extensionProperties(propertyCount);
			vkEnumerateInstanceExtensionProperties(nullptr, &propertyCount, extensionProperties.data());

			for(const auto &properties : extensionProperties) {
				for(auto it = suggestedInstanceExtensions.begin(); it != suggestedInstanceExtensions.end(); ++it) {
					const char *suggested = *it;
					if(strcmp(properties.extensionName, suggested) == 0) {
						requiredInstanceExtensions.push_back(suggested);
						suggestedInstanceExtensions.erase(it);
						break;
					}
				}
			}
		}

		std::vector<const char*> requiredInstanceLayers;
		{
			uint32_t propertyCount = 0;
			vkEnumerateInstanceLayerProperties(&propertyCount, nullptr);
			std::vector<VkLayerProperties> layerProperties(propertyCount);
			vkEnumerateInstanceLayerProperties(&propertyCount, layerProperties.data());

			for(const auto &properties : layerProperties) {
				for(auto it = suggestedInstanceLayers.begin(); it != suggestedInstanceLayers.end(); ++it) {
					const char *suggested = *it;
					if(strcmp(properties.layerName, suggested) == 0) {
						requiredInstanceLayers.push_back(suggested);
						suggestedInstanceLayers.erase(it);
						break;
					}
				}
			}
		}

		for(size_t i = 0; i < requiredInstanceExtensions.size(); ++i)
			DEBUG("Extension #" << i << ": " << requiredInstanceExtensions[i]);

		for(size_t i = 0; i < requiredInstanceLayers.size(); ++i)
			DEBUG("Layer #" << i << ": " << requiredInstanceLayers[i]);

		VkInstanceCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		createInfo.flags = 0;
		createInfo.enabledExtensionCount = (uint32_t)requiredInstanceExtensions.size();
		createInfo.ppEnabledExtensionNames = requiredInstanceExtensions.data();
		createInfo.enabledLayerCount = (uint32_t)requiredInstanceLayers.size();
		createInfo.ppEnabledLayerNames = requiredInstanceLayers.data();

		CHECKVK(
			vkCreateInstance(&createInfo, nullptr, &this->instance),
			"Failed to create vulkan instance"
		);
		
		deinit([this]() {
			vkDestroyInstance(this->instance, nullptr);
		});

#if VALIDATION_LAYERS_ENABLED
    /* Load VK_EXT_debug_report entry points in debug builds */
    PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT =
        (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
    // PFN_vkDebugReportMessageEXT vkDebugReportMessageEXT =
    //     (PFN_vkDebugReportMessageEXT) vkGetInstanceProcAddr(instance, "vkDebugReportMessageEXT");
    PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT =
        (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
	
	if(vkCreateDebugReportCallbackEXT == nullptr) return;
	
	/* Setup callback creation information */
    VkDebugReportCallbackCreateInfoEXT callbackCreateInfo;
    callbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
    callbackCreateInfo.pNext = nullptr;
    callbackCreateInfo.flags =
		VK_DEBUG_REPORT_ERROR_BIT_EXT |
		VK_DEBUG_REPORT_WARNING_BIT_EXT |
        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
    callbackCreateInfo.pfnCallback = &debugReportCallback;
    callbackCreateInfo.pUserData = nullptr;

    /* Register the callback */
    VkDebugReportCallbackEXT callback;
    CHECKVK(
		vkCreateDebugReportCallbackEXT(instance, &callbackCreateInfo, nullptr, &callback),
		"Failed to create debug report callback"
	);

	if(vkDestroyDebugReportCallbackEXT == nullptr) return;
	deinit([this, callback, vkDestroyDebugReportCallbackEXT]() {
		vkDestroyDebugReportCallbackEXT(this->instance, callback, nullptr);
	});
#endif
	}

	void Engine::initializeWindow() {
		DEBUG_FUNC_SHORT();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
		window = glfwCreateWindow(this->appInfo.width, this->appInfo.height, this->appInfo.name.data(), nullptr, nullptr);
		CHECK(window != nullptr, "Could not create window.");
		
		deinit([this]() {
			glfwDestroyWindow(this->window);
		});
	}

	VkExtent2D Engine::getFramebufferSize()
	{
		int width, height;
		glfwGetFramebufferSize(this->window, &width, &height);
		return { (uint32_t)width, (uint32_t)height };
	}

	void Engine::setTitle(const std::string &title) {
		std::string out = this->appInfo.name.data() + (" - " + title);
		glfwSetWindowTitle(this->window, out.c_str());
	}

	void SyncObjects::initialize() {
		VkSemaphoreCreateInfo semaphoreInfo {};
	    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		VkFenceCreateInfo fenceInfo {};
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
		CHECKVK(vkCreateSemaphore(this->engine.device, &semaphoreInfo, nullptr, &this->availableSemaphore), "Failed to create 'image available' semaphore");
		CHECKVK(vkCreateSemaphore(this->engine.device, &semaphoreInfo, nullptr, &this->renderedSemaphore), "Failed to create 'render finished' semaphore");
		CHECKVK(vkCreateFence(this->engine.device, &fenceInfo, nullptr, &this->frameFence), "Failed to create 'in-flight' fence");
	}
	
	void SyncObjects::deinitialize() {
		vkDestroySemaphore(this->engine.device, this->availableSemaphore, nullptr);
		vkDestroySemaphore(this->engine.device, this->renderedSemaphore, nullptr);
		vkDestroyFence(this->engine.device, this->frameFence, nullptr);
	}

	void SyncObjects::waitFrame() {
		vkWaitForFences(this->engine.device, 1, &this->frameFence, VK_TRUE, UINT64_MAX);
		vkResetFences(this->engine.device, 1, &this->frameFence);
	}

	void Queue::submitCommandBuffer(CommandBuffer &commandBuffer, SyncObjects &so) {
		VkSemaphore waitSemaphores[] = { so.availableSemaphore }; // might be more here
		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		VkSubmitInfo submitInfo {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer.commandBuffer;
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &so.renderedSemaphore;
		CHECKVK_NOLOG(vkQueueSubmit(this->queue, 1, &submitInfo, so.frameFence), "Failed to submit command buffer");
	}

	void Queue::submitCommandBuffer(CommandBuffer &commandBuffer) {
		VkSubmitInfo submitInfo {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer.commandBuffer;
		CHECKVK_NOLOG(vkQueueSubmit(this->queue, 1, &submitInfo, VK_NULL_HANDLE), "Failed to submit command buffer (w/o sync)");
	}
	
	void Queue::waitIdle() { vkQueueWaitIdle(this->queue); }

	void Queue::present(Swapchain &swapchain, uint32_t imageIndex, SyncObjects &so) {
		VkPresentInfoKHR presentInfo{};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &so.renderedSemaphore;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &swapchain.swapchain;
		presentInfo.pImageIndices = &imageIndex;
		presentInfo.pResults = nullptr;
		vkQueuePresentKHR(this->queue, &presentInfo);
	}
}
