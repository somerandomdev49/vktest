#include "commands.hpp"
#include "engine.hpp"
#include "descriptors.hpp"
#include "vulkan/vulkan_core.h"
#include <algorithm>
#include <vector>

namespace t2 {
	void CommandPool::initialize(uint32_t queueFamily) {
		VkCommandPoolCreateInfo poolInfo{};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		poolInfo.queueFamilyIndex = queueFamily;
		CHECKVK(vkCreateCommandPool(this->engine.device, &poolInfo, nullptr, &this->commandPool), "Failed to create a command pool");
	}

	void CommandPool::deinitialize() {
		vkDestroyCommandPool(this->engine.device, this->commandPool, nullptr);
	}

	void CommandPool::freeCommandBuffer(CommandBuffer &commandBuffer) {
		vkFreeCommandBuffers(this->engine.device, this->commandPool, 1, &commandBuffer.commandBuffer);
	}

	void CommandBuffer::initialize(CommandPool &pool) {
		VkCommandBufferAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = pool.commandPool;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = 1;
		CHECKVK(vkAllocateCommandBuffers(this->engine.device, &allocInfo, &this->commandBuffer), "Failed to allocate command buffer");
	}

	void CommandBuffer::reset() {
		vkResetCommandBuffer(this->commandBuffer, 0);
	}

	void CommandBuffer::begin() {
		VkCommandBufferBeginInfo beginInfo {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = 0;
		beginInfo.pInheritanceInfo = nullptr;
		CHECKVK_NOLOG(vkBeginCommandBuffer(this->commandBuffer, &beginInfo), "Failed to start recording command buffer");
	}

	void CommandBuffer::cmdBeginRenderPass(Framebuffer &framebuffer, VkRect2D renderArea, u::span<const VkClearValue> clearValues) {
		VkRenderPassBeginInfo renderPassInfo {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = framebuffer.renderPass.renderPass;
		renderPassInfo.framebuffer = framebuffer.framebuffer;
		renderPassInfo.renderArea = renderArea;
		renderPassInfo.clearValueCount = clearValues.size();
		renderPassInfo.pClearValues = clearValues.data();
		vkCmdBeginRenderPass(this->commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	}

	void CommandBuffer::cmdBindPipeline(Pipeline &pipeline) {
		vkCmdBindPipeline(this->commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline);
	}

	void CommandBuffer::cmdPipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, ImageMemoryBarrier &barrier) {
		VkImageMemoryBarrier memoryBarrier{};
		memoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		memoryBarrier.srcAccessMask = barrier.srcAccessMask;
		memoryBarrier.dstAccessMask = barrier.dstAccessMask;
		memoryBarrier.srcQueueFamilyIndex = barrier.srcQueueFamilyIndex;
		memoryBarrier.dstQueueFamilyIndex = barrier.dstQueueFamilyIndex;
		memoryBarrier.oldLayout = barrier.oldLayout;
		memoryBarrier.newLayout = barrier.newLayout;
		memoryBarrier.image = barrier.image.image;
		memoryBarrier.subresourceRange = barrier.range;
		vkCmdPipelineBarrier(this->commandBuffer, srcStageMask, dstStageMask, 0, 0, nullptr, 0, nullptr, 1, &memoryBarrier);
	}

	void CommandBuffer::cmdBindDescriptorSets(
		const Pipeline &pipeline,
		uint32_t firstSet,
		u::pspan<const DescriptorSet> sets,
		u::span<const uint32_t> offsets
	) {
		std::vector<VkDescriptorSet> sets2(sets.size());
		std::transform(sets.begin(), sets.end(), sets2.begin(), [](auto set){ return set->set; });
		vkCmdBindDescriptorSets(
			this->commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipeline.pipelineLayout,
			firstSet, sets2.size(), sets2.data(),
			offsets.size(), offsets.data()
		);
	}

	void CommandBuffer::cmdBindIndexBuffer(BufferBinding bufferBinding, VkIndexType indexType) {
		vkCmdBindIndexBuffer(this->commandBuffer, bufferBinding.buffer.buffer, bufferBinding.offset, indexType);
	}

	void CommandBuffer::cmdBindVertexBuffers(u::span<const BufferBinding> bufferBindings) {
		std::vector<VkBuffer> buffers(bufferBindings.count);
		std::vector<VkDeviceSize> offsets(bufferBindings.count);
		std::transform(bufferBindings.begin(), bufferBindings.end(), buffers.begin(), [](const BufferBinding &b) {
			return b.buffer.buffer;
		});
		std::transform(bufferBindings.begin(), bufferBindings.end(), offsets.begin(), [](const BufferBinding &b) {
			return b.offset;
		});
		vkCmdBindVertexBuffers(this->commandBuffer, 0, bufferBindings.count, buffers.data(), offsets.data());
	}


	void CommandBuffer::cmdDraw(size_t vertexCount, size_t instanceCount, size_t firstVertex, size_t firstInstance) {
		vkCmdDraw(this->commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance);
	}

	void CommandBuffer::cmdDrawIndexed(size_t indexCount, size_t instanceCount, size_t firstIndex, size_t vertexOffset, size_t firstInstance) {
		vkCmdDrawIndexed(this->commandBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
	}

	void CommandBuffer::cmdCopyBuffer(Buffer &src, Buffer &dst, u::span<const VkBufferCopy> copies) {
		vkCmdCopyBuffer(this->commandBuffer, src.buffer, dst.buffer, copies.size(), copies.data());
	}

	void CommandBuffer::cmdCopyBufferToImage(Buffer &src, Image &dst, VkImageLayout layout, u::span<const VkBufferImageCopy> copies) {
		vkCmdCopyBufferToImage(this->commandBuffer, src.buffer, dst.image, layout, copies.size(), copies.data());
	}

	void CommandBuffer::cmdEndRenderPass() {
		vkCmdEndRenderPass(this->commandBuffer);
	}
	
	void CommandBuffer::end() {
		CHECKVK_NOLOG(vkEndCommandBuffer(this->commandBuffer), "Failed to record command buffer");
	}
}
