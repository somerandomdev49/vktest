#include "renderpass.hpp"
#include "vulkan/vulkan_core.h"
#include "engine.hpp"
#include <algorithm>
#include <iterator>

namespace t2 {

void RenderPass::initialize(
	const std::vector<Attachment> &attachments,
	const std::vector<Subpass> &subpasses
) {
	std::vector<VkSubpassDescription> subpassDescs(subpasses.size());
	std::vector<VkSubpassDependency> subpassDeps;
	subpassDeps.reserve(subpasses.size());
	
	uint32_t subpassIndex = 0;
	std::vector<std::vector<VkAttachmentReference>>
		listOfColorAttachments,
		listOfInputAttachments,
		listOfResolveAttachments,
		listOfDepthStencilAttachments,
		listOfPreserveAttachments,
		listOfErrorAttachments;

	for(const auto &subpass : subpasses) {
		std::vector<VkAttachmentReference>
			&colorAttachments = listOfColorAttachments.emplace_back(),
			&inputAttachments = listOfInputAttachments.emplace_back(),
			&resolveAttachments = listOfResolveAttachments.emplace_back(),
			&depthStencilAttachments = listOfDepthStencilAttachments.emplace_back(),
			&preserveAttachments = listOfPreserveAttachments.emplace_back(),
			&errorAttachments = listOfErrorAttachments.emplace_back();

		DEBUG_START("Subpass #" << subpassIndex);

		for(const auto &attachment : subpass.references) {
			DEBUG("Attachment reference of type " << attachment.type << " to #" << attachment.attachment);

			VkAttachmentReference reference {};
			reference.attachment = attachment.attachment;
			reference.layout = attachment.layout;

			std::vector<VkAttachmentReference> *target = &errorAttachments;
			
			switch(attachment.type) {
			case SUBPASS_ATTACHMENT_TYPE_COLOR: target = &colorAttachments; break;
			case SUBPASS_ATTACHMENT_TYPE_INPUT: target = &inputAttachments; break;
			case SUBPASS_ATTACHMENT_TYPE_RESOLVE: target = &resolveAttachments; break;
			case SUBPASS_ATTACHMENT_TYPE_DEPTH_STENCIL: target = &depthStencilAttachments; break;
			case SUBPASS_ATTACHMENT_TYPE_PRESERVE: target = &preserveAttachments; break;
			default: break;
			}

			target->push_back(reference);
		}

		CHECK(errorAttachments.empty(), "Tried to create a subpass with unknown attachments.")
		CHECK(
			depthStencilAttachments.size() == 0 || depthStencilAttachments.size() == 1,
			"Tried to create a subpass with more than one depth/stencil attachment."
		)

		CHECK(
			resolveAttachments.size() == 0 || resolveAttachments.size() == colorAttachments.size(),
			"Tried to create a subpass where te number of resolve attachments "
			"is not zero and not equal to the number of color attachments."
		)

		VkSubpassDescription desc {};
		desc.pipelineBindPoint = subpass.bindPoint;
		desc.colorAttachmentCount = (uint32_t)colorAttachments.size();
		desc.pColorAttachments = OPTIONAL_DATA(colorAttachments);
		desc.inputAttachmentCount = (uint32_t)inputAttachments.size();
		desc.pInputAttachments = OPTIONAL_DATA(inputAttachments);
		desc.pResolveAttachments = OPTIONAL_DATA(resolveAttachments);
		desc.pDepthStencilAttachment = OPTIONAL_DATA(depthStencilAttachments);
		desc.preserveAttachmentCount = (uint32_t)preserveAttachments.size();

		std::vector<uint32_t> preserveAttachmentsIndices;
		std::transform(
			preserveAttachments.begin(),
			preserveAttachments.end(),
			std::back_inserter(preserveAttachmentsIndices),
			[](auto attachment) { return attachment.attachment; }
		);
		desc.pPreserveAttachments = OPTIONAL_DATA(preserveAttachmentsIndices);

		subpassDescs[subpassIndex] = desc;

		if(subpass.dependency.enabled) {
			VkSubpassDependency dependency;
			dependency.srcSubpass = subpass.dependency.subpass;
			dependency.dstSubpass = subpassIndex;
			dependency.srcStageMask = subpass.dependency.srcStageMask;
			dependency.dstStageMask = subpass.dependency.dstStageMask;
			dependency.srcAccessMask = subpass.dependency.srcAccessMask;
			dependency.dstAccessMask = subpass.dependency.dstAccessMask;
			dependency.dependencyFlags = subpass.dependency.dependencyFlags;
			subpassDeps.push_back(dependency);
		}

		DEBUG_END("Subpass #" << subpassIndex << " done.");
		subpassIndex += 1;
	}


	VkRenderPassCreateInfo createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	createInfo.subpassCount = (uint32_t)subpassDescs.size();
	createInfo.pSubpasses = OPTIONAL_DATA(subpassDescs);
	createInfo.attachmentCount = (uint32_t)attachments.size();
	createInfo.pAttachments = OPTIONAL_DATA(attachments);
	createInfo.dependencyCount = (uint32_t)subpassDeps.size();
	createInfo.pDependencies = OPTIONAL_DATA(subpassDeps);

	CHECKVK(vkCreateRenderPass(this->engine.device, &createInfo, nullptr, &this->renderPass), "Failed to create render pass");

	deinit([this]() {
		vkDestroyRenderPass(this->engine.device, this->renderPass, nullptr);
	});
}

}
