#include "../game.hpp"
#include "../../test2/common.hpp"

/** Voxel, 64 bits in size */
struct Voxel {
    uint64_t id : 32;
    uint64_t state : 32;
};

class VoxelApp : public App {
    entt::entity cameraEntity;

    size_t texture;
    size_t cubeMesh;
    size_t material;

    void initialize() override {
        material = game.createMaterial("shaders/vert.glsl.spv", "shaders/frag.glsl.spv");
        texture = game.createTexture("img/tiles.png");

        cameraEntity = game.createEntity();
        auto &camera = game.getRegistry().emplace<CameraComponent>(cameraEntity, CameraComponent::CameraType::ePerspective);
        camera.perspective.near = 0.1f;
        camera.perspective.far = 100.0f;
        camera.perspective.fov = 80.0f;
        game.setMainCamera(cameraEntity);

        std::array<Vertex, 8> vertices = {
            Vertex { { +.5f, +.5f, +.5f }, { 1.0f, 1.0f, 1.0f }, { 0.5f, 0.5f } },
            Vertex { { +.5f, +.5f, -.5f }, { 1.0f, 1.0f, 1.0f }, { 0.5f, 1.0f } },
            Vertex { { +.5f, -.5f, +.5f }, { 1.0f, 1.0f, 1.0f }, { 1.0f, 5.0f } },
            Vertex { { +.5f, -.5f, -.5f }, { 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
            Vertex { { -.5f, +.5f, +.5f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
            Vertex { { -.5f, +.5f, -.5f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
            Vertex { { -.5f, -.5f, +.5f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
            Vertex { { -.5f, -.5f, -.5f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f } },
        };

        std::array<uint16_t, 6> indices = {
            0, 1, 2,
            1, 2, 3,
        };

        cubeMesh = game.createMesh(
            { vertices.size(), vertices.data() },
            { indices.size(), indices.data() }
        );

        auto e = game.createEntity();
        game.getRegistry().emplace<TransformComponent>(e, TransformComponent::identity());
        game.getRegistry().emplace<GraphicsComponent>(e, material, texture, cubeMesh);
    }

    void update(float deltaTime) override {
        auto &cameraTransform = game.getRegistry().get<TransformComponent>(cameraEntity);
        float speed = 3.0f;
        if(game.isKeyDown(Key::eD)) cameraTransform.position.x += speed * deltaTime;
        if(game.isKeyDown(Key::eA)) cameraTransform.position.x -= speed * deltaTime;
        if(game.isKeyDown(Key::eS)) cameraTransform.position.z += speed * deltaTime;
        if(game.isKeyDown(Key::eW)) cameraTransform.position.z -= speed * deltaTime;
        if(game.isKeyDown(Key::eSpace)) cameraTransform.position.y += speed * deltaTime;
        if(game.isKeyDown(Key::eLeftShift)) cameraTransform.position.y -= speed * deltaTime;

    }

    void deinitialize() override {

    }
    
public:
    VoxelApp(Game &game) : App(game) {}
};

REGISTER_APP(VoxelApp, "VoxelApp");
