#pragma once
#include "common.hpp"
#include "pipeline.hpp"
#include "swapchain.hpp"
#include "allocator.hpp"
#include "vulkan/vulkan_core.h"

namespace t2 {
	struct CommandPool {
		Engine &engine;
		VkCommandPool commandPool;

		CommandPool(Engine &engine) : engine(engine) {}
		
		void initialize(uint32_t queueFamily);
		void deinitialize();
		void freeCommandBuffer(CommandBuffer &commandBuffer);
	};

	struct BufferBinding {
		Buffer &buffer;
		VkDeviceSize offset;
	};

	struct ImageMemoryBarrier {
		Image &image;
		VkAccessFlags srcAccessMask, dstAccessMask;
		VkImageLayout oldLayout, newLayout;
		uint32_t srcQueueFamilyIndex, dstQueueFamilyIndex;
		VkImageSubresourceRange range;
	};

	struct CommandBuffer {
		Engine &engine;
		VkCommandBuffer commandBuffer;

		CommandBuffer(Engine &engine) : engine(engine) {}

		void initialize(CommandPool &pool);

		void resetAndBegin() { reset(); begin(); }
		void reset();
		void begin();
		void cmdBeginRenderPass(Framebuffer &framebuffer, VkRect2D renderArea, u::span<const VkClearValue> clearValues);
		void cmdBindPipeline(Pipeline &pipeline);
		void cmdPipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, ImageMemoryBarrier &barrier);
		void cmdBindDescriptorSets(const Pipeline &pipeline, uint32_t firstSet, u::pspan<const DescriptorSet> sets, u::span<const uint32_t> offsets = {0});
		void cmdBindVertexBuffers(u::span<const BufferBinding> buffers);
		void cmdBindIndexBuffer(BufferBinding buffer, VkIndexType indexType);
		void cmdBindVertexBuffer(BufferBinding buffer) { cmdBindVertexBuffers({ 1, &buffer }); }
		void cmdDraw(size_t vertexCount, size_t instanceCount = 1, size_t firstVertex = 0, size_t firstInstance = 0);
		void cmdDrawIndexed(size_t indexCount, size_t instanceCount = 1, size_t firstIndex = 0, size_t vertexOffset = 0, size_t firstInstance = 0);
		void cmdCopyBuffer(Buffer &src, Buffer &dst, u::span<const VkBufferCopy> copies);
		void cmdCopyBufferToImage(Buffer &src, Image &dst, VkImageLayout layout, u::span<const VkBufferImageCopy> copies);
		void cmdEndRenderPass();
		void end();
	};
}

