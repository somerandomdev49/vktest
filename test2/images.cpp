#include "images.hpp"
#include "engine.hpp"

namespace t2 {
	void ImageView::initialize(VkImageViewType type, VkFormat format, VkImageSubresourceRange range) {
		VkImageViewCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = this->image.image;
		createInfo.viewType = type;
		createInfo.format = format;
		createInfo.subresourceRange = range;

		DEBUG("&this->image " << &this->image);
		DEBUG("this->image.image " << this->image.image);
		CHECKVK(
			vkCreateImageView(this->engine.device, &createInfo, nullptr, &this->view),
			"Failed to create image view"
		);
	}

	void ImageView::deinitialize() {
		vkDestroyImageView(this->engine.device, this->view, nullptr);
	}

	void ImageSampler::initialize(const ImageSamplerInfo &info) {
		VkSamplerCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		createInfo.magFilter = info.filter.mag;
		createInfo.minFilter = info.filter.min;
		createInfo.mipmapMode = info.mipmapMode;
		createInfo.addressModeU = info.addressMode.u;
		createInfo.addressModeV = info.addressMode.v;
		createInfo.addressModeW = info.addressMode.w;
		createInfo.mipLodBias = info.lod.bias;
		createInfo.anisotropyEnable = info.anisotropy.enable;
		createInfo.maxAnisotropy = info.anisotropy.max;
		createInfo.compareEnable = info.compare.enable;
		createInfo.compareOp = info.compare.op;
		createInfo.minLod = info.lod.min;
		createInfo.maxLod = info.lod.max;
		createInfo.borderColor = info.borderColor;
		createInfo.unnormalizedCoordinates = VK_FALSE;

		CHECKVK(
			vkCreateSampler(this->engine.device, &createInfo, nullptr, &this->sampler),
			"Failed to create image sampler"
		);
	}

	void ImageSampler::deinitialize() {
		vkDestroySampler(this->engine.device, this->sampler, nullptr);
	}
}
