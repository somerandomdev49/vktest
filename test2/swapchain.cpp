#include "swapchain.hpp"
#include "vulkan/vulkan_core.h"
#include "engine.hpp"
#include <algorithm>
#include <limits>

namespace t2
{

void Swapchain::initialize() {
	selectSurfaceFormat();
	selectPresentMode();
	selectExtent();
	initializeSwapchain();
	initializeImages();
	initializeImageViews();
}

void Swapchain::selectSurfaceFormat() {
	const auto &formats = this->engine.physicalDevice.surfaceSupport.formats;

	for(const auto &format : formats) {
		if(format.format == VK_FORMAT_B8G8R8A8_SRGB
		&& format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			this->surfaceFormat = format;
			return;
		}
	}
	
	this->surfaceFormat = formats[0];
}

void Swapchain::selectPresentMode() {
	const auto &modes = engine.physicalDevice.surfaceSupport.presentModes;

    for(const auto& mode : modes) {
        if(mode == VK_PRESENT_MODE_MAILBOX_KHR) {
            this->presentMode = mode;
        }
    }

    this->presentMode = VK_PRESENT_MODE_FIFO_KHR;
}

void Swapchain::selectExtent() {
	const auto &capabilities = this->engine.physicalDevice.surfaceSupport.capabilities;

	// this is true when GLFW/driver set the extent.
	if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		this->extent = capabilities.currentExtent;
		return;
	}
	
	// otherwise, we must set it ourselves.
	this->extent = engine.getFramebufferSize();

	this->extent.width = std::clamp(
		this->extent.width, 
		capabilities.minImageExtent.width, 
		capabilities.maxImageExtent.width
	);
	
	this->extent.height = std::clamp(
		this->extent.height, 
		capabilities.minImageExtent.height, 
		capabilities.maxImageExtent.height
	);
}

void Swapchain::initializeSwapchain() {
	const auto &swapchainSupport = this->engine.physicalDevice.surfaceSupport;

	uint32_t imageCount = swapchainSupport.capabilities.minImageCount + 1;
	if(swapchainSupport.capabilities.maxImageCount > 0
	&& imageCount > swapchainSupport.capabilities.maxImageCount) {
		imageCount = swapchainSupport.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = this->engine.surface;
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = this->surfaceFormat.format;
	createInfo.imageColorSpace = this->surfaceFormat.colorSpace;
	createInfo.imageExtent = this->extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	const auto &indices = this->engine.physicalDevice.queueFamilies;
	uint32_t queueFamilyIndices[] = { indices.graphics, indices.present };

	if(indices.graphics != indices.present) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	} else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}

	createInfo.preTransform = swapchainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = this->presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE; // TODO

	CHECKVK(
		vkCreateSwapchainKHR(this->engine.device, &createInfo, nullptr, &this->swapchain),
		"Failed to create swapchain"
	);
	
	deinit([this]() {
		vkDestroySwapchainKHR(this->engine.device, this->swapchain, nullptr);
	});
}

void Swapchain::initializeImages() {
	uint32_t imageCount;
	vkGetSwapchainImagesKHR(this->engine.device, this->swapchain, &imageCount, nullptr);
	this->images.resize(imageCount);
	vkGetSwapchainImagesKHR(this->engine.device, this->swapchain, &imageCount, this->images.data());


}

void Swapchain::initializeImageViews() {
	this->imageViews.reserve(this->images.size());
	for(size_t i = 0; i < images.size(); ++i) {
		this->imageViews.push_back(ImageView(
			this->engine,
			ImageBase { this->images[i] }
		));
		
		this->imageViews[i].initialize(VK_IMAGE_VIEW_TYPE_2D, this->surfaceFormat.format, {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
		});

		deinit([this, i]() {
			this->imageViews[i].deinitialize();
		});
	}
}

void Swapchain::initializeFramebuffers(RenderPass &renderPass, const ImageView *depthImageView) {
	this->framebuffers.reserve(this->imageViews.size()); // this is very important. (all pointers become invalidated otherwise)
	for(size_t i = 0; i < this->imageViews.size(); i++) {
		const ImageView *views[2] = { &this->imageViews[i], depthImageView };
		this->framebuffers.push_back(Framebuffer(engine, renderPass));
		this->framebuffers.back().initialize(u::pspan<const ImageView>{ (size_t)(depthImageView ? 2 : 1), &views[0] }, this->extent);
		deinit([this, i]() {
			this->framebuffers[i].deinitialize();
		});
	}
}

uint32_t Swapchain::acquireNextImage(SyncObjects &so) {
	uint32_t index;
	vkAcquireNextImageKHR(this->engine.device, this->swapchain, std::numeric_limits<uint64_t>::max(), so.availableSemaphore, VK_NULL_HANDLE, &index);
	return index;
}

void Framebuffer::initialize(u::pspan<const ImageView> attachments, VkExtent2D extent, size_t layers) {
	std::vector<VkImageView> imageViews(attachments.size());
	std::transform(attachments.begin(), attachments.end(), imageViews.begin(), [](auto t) {
		return t->view;
	});
	
	VkFramebufferCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	createInfo.renderPass = this->renderPass.renderPass;
	createInfo.attachmentCount = imageViews.size();
	createInfo.pAttachments = imageViews.data();
	createInfo.width = extent.width;
	createInfo.height = extent.height;
	createInfo.layers = layers;

	CHECKVK(
		vkCreateFramebuffer(this->engine.device, &createInfo, nullptr, &this->framebuffer),
		"Could not create framebuffer"
	);

	deinit([this]() {
		vkDestroyFramebuffer(this->engine.device, this->framebuffer, nullptr);
	});
}

} // namespace t2
