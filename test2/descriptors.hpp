#pragma once
#include "common.hpp"
#include "vulkan/vulkan_core.h"
#include "allocator.hpp"

namespace t2 {
	enum DescriptorType {
		DESCRIPTOR_SAMPLER,
		DESCRIPTOR_COMBINED_IMAGE_SAMPLER,
		DESCRIPTOR_SAMPLED_IMAGE,
		DESCRIPTOR_STORAGE_IMAGE,
		DESCRIPTOR_UNIFORM_TEXEL_BUFFER,
		DESCRIPTOR_STORAGE_TEXEL_BUFFER,
		DESCRIPTOR_UNIFORM_BUFFER,
		DESCRIPTOR_STORAGE_BUFFER,
		DESCRIPTOR_UNIFORM_BUFFER_DYNAMIC,
		DESCRIPTOR_STORAGE_BUFFER_DYNAMIC,
		DESCRIPTOR_INPUT_ATTACHMENT,
	};

	enum ShaderStageBits {
		SHADER_VERTEX_BIT = 0x00000001,
		SHADER_TESSELLATION_CONTROL_BIT = 0x00000002,
		SHADER_TESSELLATION_EVALUATION_BIT = 0x00000004,
		SHADER_GEOMETRY_BIT = 0x00000008,
		SHADER_FRAGMENT_BIT = 0x00000010,
		SHADER_COMPUTE_BIT = 0x00000020,
		SHADER_ALL_GRAPHICS_BITS = 0x0000001F,
	};

	struct DescriptorSetLayoutBinding {
		DescriptorType type;
		uint32_t binding = 0;
		uint32_t count = 1;
		uint32_t usedIn = SHADER_ALL_GRAPHICS_BITS;
	};

	struct DescriptorSetLayout : DeInitable<void()> {
		Engine &engine;
		VkDescriptorSetLayout layout;
		std::vector<DescriptorSetLayoutBinding> bindings;
		DescriptorSetLayout(Engine &engine) : engine(engine) {}
		
		void initalize(u::span<const DescriptorSetLayoutBinding> bindings);
	};

	struct DescriptorBufferInfo {
		const Buffer &buffer;
		VkDeviceSize offset, size;
	};

	struct DescriptorImageInfo {
		const ImageView &view;
		const ImageSampler &sampler;
		VkImageLayout layout;
	};

	struct DescriptorSet {
		DescriptorPool &pool;
		const DescriptorSetLayout &layout;
		VkDescriptorSet set;

		void write(uint32_t binding, const DescriptorBufferInfo &descriptorBufferInfo);
		void write(uint32_t binding, const DescriptorImageInfo &descriptorImageInfo);
	};

    struct DescriptorPoolSizeForType {
        DescriptorType type;
        uint32_t count;
    };

	struct DescriptorPool {
		Engine &engine;
		VkDescriptorPool pool;

		DescriptorPool(Engine &engine) : engine(engine) {}

		void initialize(uint32_t setCount, u::span<const DescriptorPoolSizeForType> types);
		void deinitialize();
		DescriptorSet allocate(const DescriptorSetLayout &layout);
		// std::vector<VkDescriptorSet> allocate(u::span<const DescriptorSetLayout> layouts);
	};
}
