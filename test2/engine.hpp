#pragma once
#include "common.hpp"
#include "commands.hpp"
#include "vulkan/vulkan_core.h"

namespace t2 {

struct AppInfo {
	std::string_view name;
	uint32_t version;
	uint32_t width, height;
};

struct Queue {
	Engine *engine;
	VkQueue queue;

	void submitCommandBuffer(CommandBuffer &commandBuffer);
	void submitCommandBuffer(CommandBuffer &commandBuffer, SyncObjects &so);
	void present(Swapchain &swapchain, uint32_t imageIndex, SyncObjects &so);
	void waitIdle();
};

struct QueueFamilies {
	struct Indices {
		uint32_t graphics = std::numeric_limits<uint32_t>::max();
		uint32_t transfer = std::numeric_limits<uint32_t>::max();
		uint32_t present = std::numeric_limits<uint32_t>::max();

		bool complete() {
			return this->graphics != std::numeric_limits<uint32_t>::max()
				&& this->transfer != std::numeric_limits<uint32_t>::max()
				&& this->present != std::numeric_limits<uint32_t>::max();
		}
	};

	struct Queues {
		Queue graphics;
		Queue transfer;
		Queue present;
	};
};

struct SurfaceSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};

struct PhysicalDevice {
	static inline const std::vector<const char*> requiredDeviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	VkPhysicalDeviceProperties properties;
	SurfaceSupportDetails surfaceSupport;
	QueueFamilies::Indices queueFamilies;
	std::vector<VkExtensionProperties> availableExtensions;
	VkPhysicalDevice device;

	bool suitable() {
		std::set<std::string> requiredExtensions(requiredDeviceExtensions.begin(), requiredDeviceExtensions.end());

		for(const auto& extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}

		return this->queueFamilies.complete()
		    && requiredExtensions.empty()
			&& !surfaceSupport.formats.empty()
			&& !surfaceSupport.presentModes.empty()
			;
	}
};

struct Engine : DeInitable<void()> {
	AppInfo appInfo;

	VkInstance instance;
	PhysicalDevice physicalDevice;
	VkDevice device;
	VkSurfaceKHR surface;
	QueueFamilies::Queues queues;
	
	GLFWwindow *window;

	Engine(AppInfo appInfo) : appInfo(std::move(appInfo)) {}
	
	void initialize();

	float getTime() { return glfwGetTime(); }
	bool shouldEnd() { return glfwWindowShouldClose(window); }
	void frame() { glfwPollEvents(); }
	void setTitle(const std::string &title);

	VkExtent2D getFramebufferSize();
private:
	void initializeLogicalDevice();
	std::vector<const char*> selectPhysicalDevice();
	void initializeSurface();
	void initializeInstance();
	void initializeWindow();
};	

struct SyncObjects : DeInitable<void()> {
	Engine &engine;
	VkSemaphore availableSemaphore;
	VkSemaphore renderedSemaphore;
	VkFence frameFence;
	
	SyncObjects(Engine &engine) : engine(engine) {}

	void initialize();
	void deinitialize();
	void waitFrame();
};

} // namespace t2
