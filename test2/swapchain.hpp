#pragma once
#include "common.hpp"
#include "renderpass.hpp"
#include "vulkan/vulkan_core.h"
#include "images.hpp"

#include "span.hpp"

namespace t2 {
	struct Framebuffer : DeInitable<void()> {
		Engine &engine;
		RenderPass &renderPass;
		VkFramebuffer framebuffer;

		Framebuffer(Engine &engine, RenderPass &renderPass) : engine(engine), renderPass(renderPass) {}

		void initialize(u::pspan<const ImageView> attachments, VkExtent2D extent, size_t layers = 1);
	};

	struct Swapchain : DeInitable<void()> {
		Engine &engine;
		VkSwapchainKHR swapchain;
		VkPresentModeKHR presentMode;
		VkSurfaceFormatKHR surfaceFormat;
		VkExtent2D extent;

		std::vector<VkImage> images;
		std::vector<ImageView> imageViews;
		std::vector<Framebuffer> framebuffers;

		Swapchain(Engine &engine) : engine(engine) {}

		void initialize();
		void initializeFramebuffers(RenderPass &renderPass, const ImageView *depthImage = nullptr);

		uint32_t acquireNextImage(SyncObjects &so);
	private:
		void selectSurfaceFormat();
		void selectPresentMode();
		void selectExtent();
		void initializeSwapchain();
		void initializeImages();
		void initializeImageViews();
	};
}
