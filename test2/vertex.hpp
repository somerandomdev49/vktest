#pragma once
#include "common.hpp"
#include "glm/fwd.hpp"
#include "vulkan/vulkan_core.h"

namespace t2 {
	namespace detail::vertex {
		template<typename U> struct Format { static constexpr VkFormat format = (VkFormat)0; };
#define FORMAT(U, F) template<> struct Format<U> { static constexpr VkFormat format = F; };
		FORMAT(glm::fvec1, VK_FORMAT_R32_SFLOAT);
		FORMAT(glm::fvec2, VK_FORMAT_R32G32_SFLOAT);
		FORMAT(glm::fvec3, VK_FORMAT_R32G32B32_SFLOAT);
		FORMAT(glm::fvec4, VK_FORMAT_R32G32B32A32_SFLOAT);
		FORMAT(glm::ivec1, VK_FORMAT_R32_SINT);
		FORMAT(glm::ivec2, VK_FORMAT_R32G32_SINT);
		FORMAT(glm::ivec3, VK_FORMAT_R32G32B32_SINT);
		FORMAT(glm::ivec4, VK_FORMAT_R32G32B32A32_SINT);
		FORMAT(glm::uvec1, VK_FORMAT_R32_UINT);
		FORMAT(glm::uvec2, VK_FORMAT_R32G32_UINT);
		FORMAT(glm::uvec3, VK_FORMAT_R32G32B32_UINT);
		FORMAT(glm::uvec4, VK_FORMAT_R32G32B32A32_UINT);
#undef FORMAT

		template<typename T, typename ...Us>
		struct Setter { };

		template<typename T>
		struct Setter<T> {
			static constexpr inline void set(T &descriptions, size_t index = 0, size_t offset = 0) {}
		};

		template<typename T, typename U, typename ...Rest>
		struct Setter<T, U, Rest...> {
			static constexpr inline void set(T &descriptions, size_t index = 0, size_t offset = 0) {
				descriptions[index].binding = 0;
				descriptions[index].location = index;
				descriptions[index].format = Format<U>::format;
				descriptions[index].offset = offset;
				Setter<T, Rest...>::set(descriptions, index + 1, offset + sizeof(U));
			}
		};
	}

	template<typename ...Ts>
	struct VertexBase {
		using AttributeDescriptions = std::array<VkVertexInputAttributeDescription, sizeof...(Ts)>;

		static constexpr inline VkVertexInputBindingDescription getBindingDescription() {
			VkVertexInputBindingDescription bindingDescription {};
			bindingDescription.binding = 0;
			bindingDescription.stride = (sizeof(Ts) + ...);
			bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
			return bindingDescription;
		}

		static constexpr inline AttributeDescriptions getAttributeDescriptions() {
			AttributeDescriptions attributeDescriptions {};
			detail::vertex::Setter<AttributeDescriptions, Ts...>::set(attributeDescriptions);
			return attributeDescriptions;
		}
	};
}
