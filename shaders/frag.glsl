#version 450
layout(location = 0) in vec3 inColor;
layout(location = 1) in vec2 inTexCoord;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler2D uTexSampler;

void main() {
    // outColor = vec4(inColor, 1.0);
    // outColor = vec4(inTexCoord, 1.0, 1.0);
    outColor = texture(uTexSampler, inTexCoord);
    if(outColor.a < 0.1) discard;
}

