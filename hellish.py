from contextlib import redirect_stdout
from io import StringIO as __StringIO, UnsupportedOperation
from os import environ as _environ
from glob import glob as _glob
import os
from sys import stdout, stderr
from typing import TextIO as __TextIO

class StringOutput(str):
    def __rshift__(self, out) -> "StringOutput":
        self.wrote = True
        if isinstance(out, str):
            with open(out, "a") as fout:
                fout.write(self)
        else:
            fout: __TextIO = out
            fout.write(self)
            try:
                fout.flush() # should this be here?
            except UnsupportedOperation:
                pass
        return self
    
    def __gt__(self, out) -> "StringOutput":
        self.wrote = True
        if isinstance(out, str):
            with open(out, "w") as fout:
                fout.write(self)
        else:
            fout: __TextIO = out
            try:
                fout.truncate(0)
                fout.seek(0)
                fout.write(self)
                try:
                    fout.flush() # should this be here?
                except UnsupportedOperation:
                    pass
            except UnsupportedOperation:
                fout.write(self)
        return self
    
    def __del__(self):
        if not hasattr(self, "wrote") or not self.wrote:
            stdout.write(self)

class MaybeSetString(str):
    def __imatmul__(self, value: str):
        return self if self else value

class __E:
    def __getattr__(self, __name: str) -> str:
        return MaybeSetString(_environ[__name] if
            __name in _environ and _environ[__name].strip() else "")
        
    def __setattr__(self, __name: str, __value: str) -> None:
        _environ[__name] = __value

E = __E()

def redirectable(f):
    def wrapper(*args, **kwargs):
        io = __StringIO('')
        with redirect_stdout(io):
            f(*args, **kwargs)
        return StringOutput(io.getvalue())
    return wrapper

@redirectable
def print(*args, sep=' '): __builtins__["print"](*args, sep=sep)

@redirectable
def exec(cmd: str):
    print(cmd)
    os.system(cmd)

def glob(pattern: str):
    return list(map(lambda s: s.replace('\\', '/'), _glob(pattern)))
