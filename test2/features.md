# features

[the thing](https://www.khronos.org/assets/uploads/developers/library/2016-vulkan-devday-uk/10-Porting-to-Vulkan.pdf)

# todo

`SyncObjects` should have a semaphore for each attachment. So `RenderPass` -> `Framebuffer` -> `SyncObjects` -> `submitCommandBuffer()`

## pipeline settings

* viewport
  * scissor
  * ....
* rasterization state
  * cull (back, front, both, none)
  * lineWidth (*1.0f*, ...)
  * polygonMode (*fill*, line)
  * discard enable (*false*, true)
* multisampling
  * ...
* depth/stencil testing
* color blending
  * enabled (*false*, true)
  * factors, oops, ...
* _dynamic state?_
* layout
  * (!) dependent on shader
  * extracts data from shader provied
* render passes

## framebuffers

* framebuffer
  * needs renderpass
  * needs image view
    * needs swapchain

## renderer

* owns pipelines (+ cache), access through requests, created on demand
* owns vertex buffers, access through requests, created when asked

## possible way to do stuff

page 256 of the thing

```swift
struct Framebuffer {
  ...
  let renderPass: RenderPass&;
}

struct RenderPassDescription { ??? }
struct FramebufferDescription { ??? }

func createFramebufferDesc(desc: RenderPassDescription) -> FramebufferDescription {
  ...
}

func requestFramebuffer(desc: FramebufferDescription) {
  if framebufferExists(desc) {
    return getExistingFramebuffer(desc)
  }

  let framebuffer = createFramebuffer(desc)
  saveFramebuffer(desc, framebuffer)
  return framebuffer
}

func requestRenderPass(desc: RenderPassDescription) {
  if renderPassExists(desc) {
    return getExistingRenderPass(desc)
  }

  let framebuffer = requestFramebuffer(createFramebufferDesc(desc))
  let renderPass = createRenderPass(framebuffer, desc)
  saveRenderPass(desc, renderPass)
  return renderPass
}

func beginRender(desc: RenderPassDescription) {
  let framebuffer = requestFramebuffer(desc)
  cmdBeginRenderPass(framebuffer, framebuffer.renderPass)
}

```
## multithreading

record cmdbufs (have something like `CommandBuffer`) which after recording
flushes (`CommandBuffer::flush()`) into the main thread which then makes it
a secondary command buffer or joins the buffers. A single `CommandBuffer`
should probably have a single pipeline/material bound to it so the main thread
can shuffle the order of the buffers and change the pipelines only when needed.

have a thread # in the renderable component that specifies in which thread it
should be rendered in.

thread # is determined automatically by the thread manager (which evenly
distributes the entities on all of the Savailable threads)

so the loop would go like so:
```
|          Main Thread       Thread #1         Thread #2         Thread #3
|              o                o                 o                  o     
|              |                |                 |                  |
|     Ask all renderables       |                 |                  |
|     for their thread id       |                 |                  |
|              |                |                 |                  |
|     Move the entities to      |                 |                  |
|     their corresponding       |                 |                  |
|     threads, forming          |                 |                  |
|     circular queues           |                 |                  |
|              |                |                 |                  |
|              |                |                 |                  |
|              |                |                 |                  |
|              `----------------.-.---------------.-.----------------.-.
|              |                | |               | |                | |
|              |                | |               | |                | |
|              |                | |               | |                | |
| (begin) - - -|- - - - - - - - | | - - - - - - - | | - - - - - - - -| |
|              |                | |               | |                | |
| (gen cmds) - |- - - - - - - - | | - - - - - - - | | - - - - - - - -| |
|              |                |*|               |*|                |*|
|              | _______________/ |  _____________/ |  ______________/ |
|              |/             \_____/           \_____/              | |
|           submit              | |               | |                | |
|              |                | |               | |                | |
|              |                | |               | |                | |
| (update) - - |- - - - - - - - | | - - - - - - - | | - - - - - - - -| |
|              |                | |               | |                | |
|              |                | |               | |                | |
|          new entity           | |               | |                | |
|              | `------------. | | .-----------. | | .------------. | |
|              |               \___/             \___/              \| |
|              |                | |               | |                \ |
|              |                | |               | |                .||
|              |                | |               | |                |||
|              |                | |               | |                |||
|              |                | |               | |                |||
```
