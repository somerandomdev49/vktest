#pragma once
#include "common.hpp"
#include "vulkan/vulkan_core.h"

namespace t2 {
	enum AttachmentReferenceType {
		SUBPASS_ATTACHMENT_TYPE_COLOR,
		SUBPASS_ATTACHMENT_TYPE_INPUT,
		SUBPASS_ATTACHMENT_TYPE_RESOLVE,
		SUBPASS_ATTACHMENT_TYPE_DEPTH_STENCIL,
		SUBPASS_ATTACHMENT_TYPE_PRESERVE,
	};

	// struct Attachment {
	// 	VkFormat imageFormat;
	// 	VkSampleCountFlags samples;
	// 	VkAttachmentStoreOp colorStoreOp;
	// 	VkAttachmentLoadOp colorLoadOp;
	// 	VkAttachmentStoreOp stencilStoreOp;
	// 	VkAttachmentLoadOp stencilLoadOp;
	// 	VkImageLayout initialLayout;
	// 	VkImageLayout finalLayout;
	// };
	using Attachment = VkAttachmentDescription;

	struct AttachmentReference {
		AttachmentReferenceType type;
		uint32_t attachment;
		VkImageLayout layout;
	};

	enum SubpassType {
		SUBPASS_TYPE_EXTERNAL = -1
	};

	struct Dependency {
		bool enabled;
		uint32_t subpass;
		VkPipelineStageFlags srcStageMask;
		VkPipelineStageFlags dstStageMask;
		VkAccessFlags srcAccessMask;
		VkAccessFlags dstAccessMask;
		VkDependencyFlags dependencyFlags;
	};

	struct Subpass {
		VkPipelineBindPoint bindPoint;
		std::vector<AttachmentReference> references;
		Dependency dependency;
	};

	struct RenderPass : DeInitable<void()> {
		Engine &engine;
		VkRenderPass renderPass;

		RenderPass(Engine &engine) : engine(engine) {}

		void initialize(
			const std::vector<Attachment> &attachments,
			const std::vector<Subpass> &subpasses
		);
	};
}
