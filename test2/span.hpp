#pragma once
#include <iterator>

namespace u {
	template<typename C, typename U>
	struct generic_iterator {
		C &container;
		size_t index;

		generic_iterator<C, U> &operator++() { ++index; return *this; }
		generic_iterator<C, U> &operator--() { --index; return *this; }
		bool operator==(const generic_iterator<C, U> &other) const { return other.index == this->index; }
		bool operator!=(const generic_iterator<C, U> &other) const { return !(*this == other); }
		U &operator->() { return container[index]; }
		U &operator*() { return container[index]; }
	};

	template<typename T>
	struct span {
		size_t count;
		T *values;

		size_t size() const { return count; }
		T *data() const { return values; }

		using iterator = generic_iterator<span, T>;
		using const_iterator = generic_iterator<const span, const T>;

		const T &operator[](size_t index) const {
			return values[index];
		}

		T &operator[](size_t index) {
			return values[index];
		}

		iterator begin() { return { *this, 0 }; }
		iterator end() { return { *this, count }; }
		const_iterator begin() const { return { *this, 0 }; }
		const_iterator end() const { return { *this, count }; }
		const_iterator cbegin() const { return { *this, 0 }; }
		const_iterator cend() const { return { *this, count }; }
	};

	template<typename T>
	using pspan = span<T*>;
}

template<typename C, typename T>
struct std::iterator_traits<u::generic_iterator<C, T>> {
	using value_type = T;
	using reference_type = T&;
	using iterator_category = std::bidirectional_iterator_tag;
};
