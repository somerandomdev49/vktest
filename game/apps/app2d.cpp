#include "../game.hpp"
#include "../../test2/common.hpp"

struct PlayerComponent { float speed; float timer; };
struct ProjectileComponent { glm::vec3 delta; float age; };

struct Rect {
    glm::vec2 pos, size;

    bool checkInside(glm::vec2 v) const {
        return v.x > pos.x && v.x < pos.x + size.x
            && v.y > pos.y && v.y < pos.y + size.y;
    }
};

// TODO: fix bug with mouse position when pressing keyboard
class App2D : public App {
    size_t mesh;
    size_t material;
    size_t playerTexture;
    size_t projectileTexture;

    entt::entity camera;

    void initialize() override {
        material = game.createMaterial("shaders/vert.glsl.spv", "shaders/frag.glsl.spv");
        playerTexture = game.createTexture("img/o.png");
        projectileTexture = game.createTexture("img/projectile.png");

        Vertex vertices[] = {
            { { -0.5f, -0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } },
            { {  0.5f, -0.5f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
            { {  0.5f,  0.5f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f } },
            { { -0.5f,  0.5f, 0.0f }, { 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
        };

        uint16_t indices[] = {
            0, 1, 2, 2, 3, 0
        };

        mesh = game.createMesh({ 4, vertices }, { 6, indices });

        DEBUG("mesh = " << mesh);

        auto a = game.getRegistry().create();
        game.getRegistry().emplace<TransformComponent>(a,
            glm::vec3{0.0f, 0.0f, 0.0f},
            glm::identity<glm::quat>(),
            glm::vec3{128.0f, 128.0f, 0.8f}
        );
        game.getRegistry().emplace<GraphicsComponent>(a, material, playerTexture, mesh);
        game.getRegistry().emplace<PlayerComponent>(a, 200.0f);

        camera = game.getRegistry().create();
        game.getRegistry().emplace<CameraComponent>(camera, CameraComponent::CameraType::ePixel);
        game.getRegistry().get<CameraComponent>(camera).pixel.scale = 1.0f;
        game.getRegistry().get<CameraComponent>(camera).pixel.offset = { 0.0f, 0.0f };
        game.setMainCamera(camera);
    }

    void update(float deltaTime) override {
        game.getRegistry().view<TransformComponent, PlayerComponent>().each([&](TransformComponent &transform, PlayerComponent &player) {
            if(game.isKeyDown(Key::eA)) transform.position.x -= player.speed * deltaTime;
            if(game.isKeyDown(Key::eD)) transform.position.x += player.speed * deltaTime;
            if(game.isKeyDown(Key::eW)) transform.position.y -= player.speed * deltaTime;
            if(game.isKeyDown(Key::eS)) transform.position.y += player.speed * deltaTime;
            if(game.isKeyDown(Key::eSpace) && player.timer >= .2f) {
                glm::vec2 mouse = game.getMousePosition();
                float angle = atan2f(transform.position.y - mouse.y, transform.position.x - mouse.x);
                glm::vec2 delta = { cosf(angle), sinf(angle) };
                auto e = game.getRegistry().create();
                game.getRegistry().emplace<TransformComponent>(e,
                    transform.position,
                    glm::quat(glm::vec3(0.0f, 0.0f, angle)),
                    glm::vec3(64.0f, 64.0f, 1.0f)
                );
                game.getRegistry().emplace<ProjectileComponent>(e, glm::vec3(delta, 0.0f), 0.0f);
                game.getRegistry().emplace<GraphicsComponent>(e, material, projectileTexture, mesh);
                player.timer = 0.0f;
            }
            player.timer += deltaTime;
        });

        auto framebufferSize = glm::vec2(game.getFramebufferSize());
#define PROJECTILE_MAX_AGE 5.0f
        game.getRegistry().view<TransformComponent, ProjectileComponent>().each([&](auto e, TransformComponent &transform, ProjectileComponent &projectile) {
            transform.position -= projectile.delta * 500.0f * deltaTime;
            projectile.delta *= 0.99f;
            projectile.age += deltaTime;
            float margin = 32.0f;
            if(!Rect{glm::vec2{-margin}, framebufferSize + margin * 2}.checkInside(transform.position)
            || projectile.age >= PROJECTILE_MAX_AGE) {
                game.getRegistry().destroy(e);
            }
        });
    }

    void deinitialize() override {

    }

public:
    App2D(Game &game) : App(game) {}
};

REGISTER_APP(App2D, "App2D");
