#pragma once
#include "common.hpp"
#include "vulkan/vulkan_core.h"

namespace t2 {
	enum CullMode {
		CULL_MODE_NONE,
		CULL_MODE_FRONT,
		CULL_MODE_BACK,
		CULL_MODE_BOTH,
	};

	enum PolygonMode {
		POLYGON_MODE_LINE,
		POLYGON_MODE_FILL,
	};

	struct PipelineInfo {
		struct LayoutInfo {
			u::pspan<const DescriptorSetLayout> descriptorSetLayouts;
		} layoutInfo;

		struct ScissorInfo {
			VkOffset2D offset;
			VkExtent2D extent;
		} scissorInfo;

		struct RasterizerInfo {
			CullMode cullMode;
			float lineWidth;
			PolygonMode polygonMode;
			bool enableDiscard;
		} rasterizerInfo;

		struct ColorBlendingInfo {
			bool enabled;
		} colorBlendingInfo;

		struct VertexInputInfo {
			u::span<const VkVertexInputAttributeDescription> attributeDescriptions;
			u::span<const VkVertexInputBindingDescription> bindingDescriptions;
		} vertexInputInfo;

		struct DepthAndStencilInfo {
			bool enableDepthTest;
			bool enableDepthWrite;
			VkCompareOp depthCompareOp;
			bool enableDepthBoundsTest;
			float minDepthBound, maxDepthBound;
			bool enableStencilTest;
		} depthAndStencilInfo;

		VkExtent2D viewportExtent;
	};

	struct Pipeline : DeInitable<void()> {
		Engine &engine;
		VkPipelineLayout pipelineLayout;
		VkPipeline pipeline;

		Pipeline(Engine &engine) : engine(engine) {}

		void initialize(const PipelineInfo &info, Shader &shader, RenderPass &renderPass);
	private:
		void initializeLayout(const PipelineInfo &info);
	};
}
