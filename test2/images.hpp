#pragma once
#include "common.hpp"
#include <vulkan/vulkan.h>

namespace t2 {
	struct ImageBase {
		VkImage image;
	};

	struct ImageView {
		Engine &engine;
		const ImageBase &image;
		VkImageView view;

		ImageView(Engine &engine, const ImageBase &image) : engine(engine), image(image) {}

		void initialize(VkImageViewType type, VkFormat format, VkImageSubresourceRange range);
		void deinitialize();
	};

	struct ImageSamplerInfo {
		VkBorderColor borderColor;
		VkSamplerMipmapMode mipmapMode;
		struct { VkFilter mag, min; } filter;
		struct { VkSamplerAddressMode u, v, w; } addressMode;
		struct { bool enable; float max; } anisotropy;
		struct { bool enable; VkCompareOp op; } compare;
		struct { float bias, min, max; } lod;
	};

	struct ImageSampler {
		Engine &engine;
		VkSampler sampler;
		
		ImageSampler(Engine &engine) : engine(engine) {}

		void initialize(const ImageSamplerInfo &info);
		void deinitialize();
	};
}
