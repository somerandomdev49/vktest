#ifdef __MINGW32__
#define NEED_COLOR_FIX
#endif

#ifdef NEED_COLOR_FIX
#include <windows.h>
#endif

void platform_prep()
{
#ifdef NEED_COLOR_FIX
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	if(handle != INVALID_HANDLE_VALUE) {
		DWORD mode = 0;
		if(GetConsoleMode(handle, &mode)) {
			mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
			SetConsoleMode(handle, mode);
		}
	}
#endif
}





