#pragma once

#include "common.hpp"
#include "engine.hpp"
#include "vulkan/vulkan_core.h"

namespace t2 {
	struct ShaderModule {
		Engine &engine;
		VkShaderModule module;
		std::string entry;

		ShaderModule(ShaderModule &&other)
			: engine(other.engine),
			  module(std::move(other.module)),
			  entry(std::move(other.entry)) {
		}
			  
		ShaderModule &operator=(ShaderModule &&other) {
			this->engine = std::move(other.engine);
			this->module = std::move(other.module);
			this->entry = std::move(other.entry);
			return *this;
		}
		
		ShaderModule(Engine &engine, std::string entry)
			: engine(engine), entry(std::move(entry)) {}

		void initialize(size_t size, uint32_t *code);
		void deinitialize();
	};

	enum ShaderType {
		SHADER_TYPE_VERTEX,
		SHADER_TYPE_FRAGMENT,
		SHADER_TYPE_COMPUTE,
		SHADER_TYPE_GEOMETRY,
	};

	struct Shader : DeInitable<void()> {
		Engine &engine;
		std::vector<ShaderModule> modules;

		struct ModuleIndices {
			size_t vertex, fragment, compute, geometry;
		} indices;

		struct ModuleEnabled {
			bool vertex, fragment, compute, geometry;
		} enabled;

		Shader(Engine &engine) : engine(engine), enabled {0} {}

		void add(ShaderType type, ShaderModule &&shaderModule);
	};
}
