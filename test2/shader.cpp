#include "shader.hpp"
#include "vulkan/vulkan_core.h"

namespace t2 {
	void ShaderModule::initialize(size_t size, uint32_t *code) {
		VkShaderModuleCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.flags = 0;
		createInfo.codeSize = size;
		createInfo.pCode = code;

		CHECKVK(
			vkCreateShaderModule(this->engine.device, &createInfo, nullptr, &this->module),
			"Failed to create a shader module"
		);
	}

	void ShaderModule::deinitialize() {
		vkDestroyShaderModule(this->engine.device, this->module, nullptr);
	}

	void Shader::add(ShaderType type, ShaderModule &&shaderModule) {
		size_t index = this->modules.size();

		bool wasPreviousEnabled = false;
		size_t previousIndex = 0;

#define DO(NAME) \
			previousIndex = this->indices.NAME; \
			wasPreviousEnabled = this->enabled.NAME; \
			this->indices.NAME = index; \
			this->enabled.NAME = true; \
			break;

		switch(type) {
		case SHADER_TYPE_VERTEX: DO(vertex);
		case SHADER_TYPE_FRAGMENT: DO(fragment);
		case SHADER_TYPE_COMPUTE: DO(compute);
		case SHADER_TYPE_GEOMETRY: DO(geometry);
		default:
			DEBUG_ERROR("Unknown shader type " << type);
			break;
		}

		if(!wasPreviousEnabled) {
			this->modules.push_back(std::move(shaderModule));

			// we don't need to deinit twice!
			// if the previous shader module was enabled,
			// it already has a deinit function for it's index.
			deinit([this, index]() mutable {
				this->modules[index].deinitialize();
			});

			return;
		}

		this->modules[previousIndex].deinitialize();
		this->modules[previousIndex] = std::move(shaderModule);
	}
}
