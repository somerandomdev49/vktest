#include "pipeline.hpp"
#include "vulkan/vulkan_core.h"

#include "shader.hpp"
#include "renderpass.hpp"
#include "descriptors.hpp"

namespace t2 {

namespace {
	VkPipelineShaderStageCreateInfo getCreateInfo(VkShaderStageFlagBits stage, const ShaderModule &module) {
		VkPipelineShaderStageCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		createInfo.module = module.module;
		createInfo.pName = module.entry.c_str();
		createInfo.stage = stage;
		return createInfo;
	}
}

void Pipeline::initialize(const PipelineInfo &info, Shader &shader, RenderPass &renderPass) {
	this->initializeLayout(info);
	DEBUG("initialized layout");

	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

#define DO(NAME, BIT) \
	if(shader.enabled.NAME) \
		shaderStages.push_back(getCreateInfo( \
			BIT, \
			shader.modules[shader.indices.NAME] \
		));

	DO(vertex, VK_SHADER_STAGE_VERTEX_BIT);
	DO(fragment, VK_SHADER_STAGE_FRAGMENT_BIT);
	DO(compute, VK_SHADER_STAGE_COMPUTE_BIT);
	DO(geometry, VK_SHADER_STAGE_GEOMETRY_BIT);
#undef DO
	VkPipelineVertexInputStateCreateInfo vertexInputInfo {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = (uint32_t)info.vertexInputInfo.bindingDescriptions.count;
	vertexInputInfo.pVertexBindingDescriptions = info.vertexInputInfo.bindingDescriptions.values;
	vertexInputInfo.vertexAttributeDescriptionCount = (uint32_t)info.vertexInputInfo.attributeDescriptions.count;
	vertexInputInfo.pVertexAttributeDescriptions = info.vertexInputInfo.attributeDescriptions.values;

	VkPipelineInputAssemblyStateCreateInfo inputAssembly {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	VkRect2D scissor{};
	scissor.offset = info.scissorInfo.offset;
	scissor.extent = info.scissorInfo.extent;

	VkViewport viewport {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float) info.viewportExtent.width;
	viewport.height = (float) info.viewportExtent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkPipelineViewportStateCreateInfo viewportState{};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	static const VkPolygonMode polygonModeConversion[] = {
		VK_POLYGON_MODE_LINE,
		VK_POLYGON_MODE_FILL,
	};

	static const VkCullModeFlagBits cullModeConversion[] = {
		VK_CULL_MODE_NONE,
		VK_CULL_MODE_FRONT_BIT,
		VK_CULL_MODE_BACK_BIT,
		VK_CULL_MODE_FRONT_AND_BACK,
	};

	VkPipelineRasterizationStateCreateInfo rasterizer {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = info.rasterizerInfo.enableDiscard;
	rasterizer.polygonMode = polygonModeConversion[info.rasterizerInfo.polygonMode];
	rasterizer.lineWidth = info.rasterizerInfo.lineWidth;
	rasterizer.cullMode = cullModeConversion[info.rasterizerInfo.cullMode];
	rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	VkPipelineMultisampleStateCreateInfo multisampling {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f;
	multisampling.pSampleMask = nullptr;
	multisampling.alphaToCoverageEnable = VK_FALSE;
	multisampling.alphaToOneEnable = VK_FALSE;

	VkPipelineColorBlendAttachmentState colorBlendAttachment {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colorBlending {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f;
	colorBlending.blendConstants[1] = 0.0f;
	colorBlending.blendConstants[2] = 0.0f;
	colorBlending.blendConstants[3] = 0.0f;

	std::vector<VkDynamicState> dynamicStates = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_LINE_WIDTH
	};

	VkPipelineDynamicStateCreateInfo dynamicState{};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
	dynamicState.pDynamicStates = dynamicStates.data();

	VkPipelineDepthStencilStateCreateInfo depthStencilState{};
	depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilState.depthTestEnable = info.depthAndStencilInfo.enableDepthTest;
	depthStencilState.depthWriteEnable = info.depthAndStencilInfo.enableDepthWrite;
	depthStencilState.depthCompareOp = info.depthAndStencilInfo.depthCompareOp;
	depthStencilState.depthBoundsTestEnable = info.depthAndStencilInfo.enableDepthBoundsTest;
	depthStencilState.minDepthBounds = info.depthAndStencilInfo.minDepthBound;
	depthStencilState.maxDepthBounds = info.depthAndStencilInfo.maxDepthBound;
	depthStencilState.stencilTestEnable = info.depthAndStencilInfo.enableStencilTest;

	VkGraphicsPipelineCreateInfo createInfo {}; // TODO: support compute and other pipeline types.
	createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	createInfo.stageCount = (uint32_t)shaderStages.size();
	createInfo.pStages = OPTIONAL_DATA(shaderStages);
	createInfo.pVertexInputState = &vertexInputInfo;
	createInfo.pInputAssemblyState = &inputAssembly;
	createInfo.pViewportState = &viewportState;
	createInfo.pRasterizationState = &rasterizer;
	createInfo.pMultisampleState = &multisampling;
	createInfo.pDepthStencilState = &depthStencilState;
	createInfo.pColorBlendState = &colorBlending;
	createInfo.pDynamicState = nullptr;
	createInfo.layout = pipelineLayout;
	createInfo.renderPass = renderPass.renderPass;
	createInfo.subpass = 0;
	createInfo.basePipelineHandle = VK_NULL_HANDLE;
	createInfo.basePipelineIndex = -1;

	CHECKVK(
		vkCreateGraphicsPipelines(this->engine.device, VK_NULL_HANDLE, 1, &createInfo, nullptr, &this->pipeline),
		"Failed to create a graphics pipeline."
	);

	deinit([this]() {
		vkDestroyPipeline(this->engine.device, this->pipeline, nullptr);
	});
}

void Pipeline::initializeLayout(const PipelineInfo &info) {
	std::vector<VkDescriptorSetLayout> layouts(info.layoutInfo.descriptorSetLayouts.size());
	std::transform(
		info.layoutInfo.descriptorSetLayouts.begin(),
		info.layoutInfo.descriptorSetLayouts.end(),
		layouts.begin(),
		[](const DescriptorSetLayout *layout) {
			DEBUG("layout: " << (void*)layout);
			return layout->layout;
		}
	);

	VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = layouts.size();
	pipelineLayoutInfo.pSetLayouts = layouts.data();
	pipelineLayoutInfo.pushConstantRangeCount = 0;
	pipelineLayoutInfo.pPushConstantRanges = nullptr;

	CHECKVK(
		vkCreatePipelineLayout(this->engine.device, &pipelineLayoutInfo, nullptr, &this->pipelineLayout),
		"Could not create the pipeline layout."
	);

	DEBUG("Created pipeline layout");

	deinit([this]() {
		vkDestroyPipelineLayout(this->engine.device, this->pipelineLayout, nullptr);
	});
}

} // namespace t2
