#include <test2/common.hpp>
#include <test2/engine.hpp>
#include <test2/swapchain.hpp>
#include <test2/platform.hpp>
#include <test2/pipeline.hpp>
#include <test2/renderpass.hpp>
#include <test2/shader.hpp>
#include <test2/vertex.hpp>
#include <test2/descriptors.hpp>
#include "vulkan/vulkan_core.h"
#include <stdexcept>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <stdint.h>
#include <entt/entity/registry.hpp>
#include "game.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


static std::vector<char> readFile(const std::string &filename) {
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    CHECK(file.is_open(), "Failed to open file: '" + std::string(filename) + "'.");

    size_t fileSize = (size_t)file.tellg();
    std::vector<char> buffer(fileSize);
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();
    return buffer;
}

t2::ShaderModule createShaderModule(t2::Engine &engine, const char *entry, const char *path) {
    t2::ShaderModule module(engine, entry);
    auto data = readFile(path);
    module.initialize(data.size(), (uint32_t*)data.data());
    return module;
}

void transitionImageLayout(t2::CommandBuffer &commandBuffer, t2::Image &image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout) {
    VkPipelineStageFlags srcStage;
    VkPipelineStageFlags dstStage;

    t2::ImageMemoryBarrier barrier { .image = image };
    if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
        throw std::invalid_argument("Unsupported layout transition");
    }

    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.range = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1,
    };

    commandBuffer.cmdPipelineBarrier(srcStage, dstStage, barrier);
}

void transferImageData(t2::Engine &engine, t2::CommandPool &commandPool, t2::Image &image, u::span<const uint8_t> data) {
    auto stagingBuffer = image.allocator->createBuffer(
        data.size(),
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        t2::ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | t2::ALLOCATION_CREATE_MAPPED_BIT
    );

    image.allocator->writeBuffer(stagingBuffer, data);

    t2::CommandBuffer commandBuffer(engine);
    commandBuffer.initialize(commandPool);
    commandBuffer.begin();
    transitionImageLayout(
        commandBuffer,
        image,
        VK_FORMAT_R8G8B8A8_SRGB,
        image.layout,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    VkBufferImageCopy copy{};
    copy.bufferOffset = 0;
    copy.bufferRowLength = 0;
    copy.bufferImageHeight = 0;

    copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    copy.imageSubresource.mipLevel = 0;
    copy.imageSubresource.baseArrayLayer = 0;
    copy.imageSubresource.layerCount = 1;

    copy.imageOffset = {0, 0, 0};
    copy.imageExtent = {
        image.extent.width,
        image.extent.height,
        1
    };

    commandBuffer.cmdCopyBufferToImage(stagingBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, { 1, &copy });
    transitionImageLayout(
        commandBuffer,
        image,
        VK_FORMAT_R8G8B8A8_SRGB,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );
    commandBuffer.end();

    engine.queues.graphics.submitCommandBuffer(commandBuffer);
    engine.queues.graphics.waitIdle();

    commandPool.freeCommandBuffer(commandBuffer);
    stagingBuffer.allocator->destroyBuffer(stagingBuffer);
}

void transferBufferData(t2::Engine &engine, t2::CommandPool &commandPool, t2::Buffer &targetBuffer, u::span<const uint8_t> data) {
    auto stagingBuffer = targetBuffer.allocator->createBuffer(
        data.size(),
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        t2::ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | t2::ALLOCATION_CREATE_MAPPED_BIT
    );

    targetBuffer.allocator->writeBuffer(stagingBuffer, data);
    VkBufferCopy copy { 0, 0, targetBuffer.size };

    t2::CommandBuffer commandBuffer(engine);
    commandBuffer.initialize(commandPool);
    commandBuffer.begin();
    commandBuffer.cmdCopyBuffer(stagingBuffer, targetBuffer, { 1, &copy });
    commandBuffer.end();

    engine.queues.graphics.submitCommandBuffer(commandBuffer);
    engine.queues.graphics.waitIdle();

    commandPool.freeCommandBuffer(commandBuffer);
    stagingBuffer.allocator->destroyBuffer(stagingBuffer);
}

template<typename T>
void transferBufferData(t2::Engine &engine, t2::CommandPool &commandPool, t2::Buffer &targetBuffer, u::span<const T> data) {
    transferBufferData(engine, commandPool, targetBuffer, { data.size() * sizeof(T), (uint8_t*)data.data() });
}

template<typename T>
struct MaybeUninit {
    uint8_t bytes[sizeof(T)];
    
    MaybeUninit() {}
    ~MaybeUninit() { get().~T(); }

    template<typename ...Args>
    MaybeUninit(Args &&...args) {
        initialize(std::forward<Args>(args)...);
    }

    template<typename ...Args>
    T &initialize(Args &&...args) {
        new((T*)this->bytes) T(std::forward<Args>(args)...);
        return get();
    }

    operator const T&() const { return get(); }
    operator T&() { return get(); }
    
    const T &get() const { return *(T*)this->bytes; }
    T &get() { return *(T*)this->bytes; }
};

struct Texture {
    MaybeUninit<t2::DescriptorSet> set;
    MaybeUninit<t2::Image> image;
    t2::ImageView view;

    Texture(t2::Engine &engine)
        : view(engine, *(t2::Image*)(&this->image)) {}
};

struct Mesh {
    t2::Buffer vertexBuffer, indexBuffer;
};

struct Material {
    t2::Shader shader;
    t2::Pipeline pipeline;
};

namespace std {
    template<>
    struct allocator<::Material> {
        typedef ::Material value_type;
        value_type* allocate(size_t n) { return static_cast<value_type*>(::operator new(sizeof(value_type) * n)); }
        void deallocate(value_type* p, size_t n) { return ::operator delete(static_cast<void*>(p)); }
        template<class U, class... Args>
        void construct(U* p, Args&&... args) { ::new(static_cast<void*>(p)) U{ std::forward<Args>(args)... }; }
    };
}

namespace std {
    template<>
    struct allocator<::Mesh> {
        typedef ::Mesh value_type;
        value_type* allocate(size_t n) { return static_cast<value_type*>(::operator new(sizeof(value_type) * n)); }
        void deallocate(value_type* p, size_t n) { return ::operator delete(static_cast<void*>(p)); }
        template<class U, class... Args>
        void construct(U* p, Args&&... args) { ::new(static_cast<void*>(p)) U{ std::forward<Args>(args)... }; }
    };
}

struct CreateTextureParams {
    t2::Engine &engine;
    t2::Allocator &allocator;
    t2::CommandPool &commandPool;
    t2::DescriptorPool &descriptorPool;
    t2::DescriptorSetLayout &descriptorSetLayout;
};

void createTexture(std::vector<Texture> &textures, const char *path, const CreateTextureParams &params) {
}

struct Game::Impl {
    Game &game;
    t2::Engine engine;
    t2::RenderPass renderPass;
    t2::Swapchain swapchain;
    t2::Allocator allocator;
    t2::CommandPool commandPool;
    t2::DescriptorPool descriptorPool;
    t2::ImageSampler imageSampler;
    MaybeUninit<t2::DescriptorSet> uniformDescriptor;
    MaybeUninit<t2::Image> depthImage;
    t2::ImageView depthView;
    t2::CommandBuffer mainCommandBuffer;
    t2::SyncObjects sync;
    size_t uniformBufferSize;
    entt::registry registry;
    std::vector<Mesh> meshes;
    std::vector<Texture> textures;
    std::vector<Material> materials;
    std::vector<t2::DescriptorSetLayout> descriptorSetLayouts;

    entt::entity mainCamera;
    friend class Game;

    Impl(Game &game, const t2::AppInfo &info)
        : game(game)
        , engine(info)
        , renderPass(engine)
        , swapchain(engine)
        , allocator(engine)
        , commandPool(engine)
        , descriptorPool(engine)
        , imageSampler(engine)
        , sync(engine)
        , mainCommandBuffer(engine)
        , mainCamera(entt::tombstone)
        , depthView(engine, depthImage) {
    }

    size_t createMesh(u::span<Vertex> vertices, u::span<uint16_t> indices) {
        // const std::vector<Vertex> vertices = {
        // 	{ { -0.5f, -0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } },
        // 	{ {  0.5f, -0.5f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
        // 	{ {  0.5f,  0.5f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f } },
        // 	{ { -0.5f,  0.5f, 0.0f }, { 1.0f, 1.0f, 1.0f }, { 1.0f, 1.0f } },
        // };

        // const std::vector<uint16_t> indices = {
        // 	0, 1, 2, 2, 3, 0
        // };

        meshes.emplace_back(
            allocator.createBuffer(
                vertices.size() * sizeof(Vertex),
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
            ),
            allocator.createBuffer(
                indices.size() * sizeof(uint16_t),
                VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
            )
        );
        
        transferBufferData<const Vertex>(engine, commandPool, meshes.back().vertexBuffer, { vertices.size(), vertices.data() });
        transferBufferData<const uint16_t>(engine, commandPool, meshes.back().indexBuffer, { indices.size(), indices.data() });
        return meshes.size() - 1;
    }

    size_t createMaterial(const char *vertexPath, const char *fragmentPath) {
        materials.emplace_back(engine, engine);
        auto &m = materials.back();
        m.shader.add(t2::SHADER_TYPE_VERTEX, createShaderModule(engine, "main", vertexPath));
        m.shader.add(t2::SHADER_TYPE_FRAGMENT, createShaderModule(engine, "main", fragmentPath));
        t2::PipelineInfo pipelineInfo;
        auto vertexAttributeDescriptions = Vertex::Info::getAttributeDescriptions();
        auto vertexBindingDescription = Vertex::Info::getBindingDescription();	
        DEBUG("setLayoutPtrs[0] = " << (void*)&descriptorSetLayouts[0]);	
        const t2::DescriptorSetLayout *setLayoutPtrs[] = { &descriptorSetLayouts[0], &descriptorSetLayouts[1] };
        pipelineInfo.layoutInfo.descriptorSetLayouts = { 2, &setLayoutPtrs[0] };
        pipelineInfo.colorBlendingInfo.enabled = false;
        pipelineInfo.rasterizerInfo.enableDiscard = false;
        pipelineInfo.rasterizerInfo.cullMode = t2::CULL_MODE_NONE;
        pipelineInfo.rasterizerInfo.polygonMode = t2::POLYGON_MODE_FILL;
        pipelineInfo.rasterizerInfo.lineWidth = 1.0f;
        pipelineInfo.scissorInfo.offset = { 0, 0 };
        pipelineInfo.scissorInfo.extent = { 0, 0 };
        pipelineInfo.viewportExtent     = { 0, 0 };
        pipelineInfo.vertexInputInfo.bindingDescriptions = { 1, &vertexBindingDescription };
        pipelineInfo.vertexInputInfo.attributeDescriptions = {
            vertexAttributeDescriptions.size(),
            vertexAttributeDescriptions.data()
        };
        pipelineInfo.depthAndStencilInfo.enableDepthTest = true;
        pipelineInfo.depthAndStencilInfo.enableDepthWrite = true;
        pipelineInfo.depthAndStencilInfo.enableDepthBoundsTest = false;
        pipelineInfo.depthAndStencilInfo.minDepthBound = 0.0f;
        pipelineInfo.depthAndStencilInfo.maxDepthBound = 1.0f;
        pipelineInfo.depthAndStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS;
        pipelineInfo.depthAndStencilInfo.enableStencilTest = false;
        pipelineInfo.scissorInfo.extent = swapchain.extent;
        pipelineInfo.viewportExtent = swapchain.extent;
        m.pipeline.initialize(pipelineInfo, m.shader, renderPass);
        return materials.size() - 1;
    }

    size_t createTexture(const char *path) {
        int imageWidth, imageHeight, imageChannels;
        uint8_t *imageData = stbi_load(path, &imageWidth, &imageHeight, &imageChannels, STBI_rgb_alpha);

        textures.emplace_back(engine);
        auto &out = textures.back();
        out.image.get() = std::move(allocator.createImage(t2::ImageInfo {
            .type = t2::IMAGE_TYPE_2D,
            .extent = { .width = (uint32_t)imageWidth, .height = (uint32_t)imageHeight, .depth = 1 },
            .mipLevels = 1,
            .arrayLayers = 1,
            .format = VK_FORMAT_R8G8B8A8_SRGB,
            .tiling = t2::IMAGE_TILING_OPTIMAL,
            .isPreinit = false,
            .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .samples = VK_SAMPLE_COUNT_1_BIT,
        }));

        out.view.initialize(VK_IMAGE_VIEW_TYPE_2D, VK_FORMAT_R8G8B8A8_SRGB, {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        });

        transferImageData(engine, commandPool, out.image.get(), {
            size_t(imageWidth * imageHeight) * 4,
            imageData
        });

        out.set = descriptorPool.allocate(descriptorSetLayouts[0]);
        out.set.get().write(0, t2::DescriptorImageInfo {
            .view = out.view,
            .sampler = imageSampler,
            .layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        });

        free(imageData);
        return textures.size() - 1;
    }

    void frame_() {
        uint32_t nextImage = swapchain.acquireNextImage(sync);
        mainCommandBuffer.reset();
        mainCommandBuffer.begin();
        VkClearValue clearValues[] = {
            VkClearValue { .color = { 0.0f, 0.0f, 0.0f, 0.0f } },
            VkClearValue { .depthStencil = { 1.0f, 0 } }
        };

        registry.sort<GraphicsComponent>([](const auto &lhs, const auto &rhs) {
            return lhs.material < rhs.material;
        });

        // TODO: instancing?
        mainCommandBuffer.cmdBeginRenderPass(swapchain.framebuffers[nextImage], {{ 0, 0 }, swapchain.extent}, { 2, clearValues });
        {
            size_t lastMaterial = (size_t)-1;
            size_t lastTexture = (size_t)-1;
            size_t lastMesh = (size_t)-1;
            size_t i = 0;
            registry.view<TransformComponent, GraphicsComponent>().each([&](
                    TransformComponent &transform,
                    GraphicsComponent &graphics
            ) {
                if(graphics.material != lastMaterial) {
                    mainCommandBuffer.cmdBindPipeline(materials[graphics.material].pipeline);
                }

                if(graphics.mesh != lastMesh) {
                    mainCommandBuffer.cmdBindIndexBuffer({ meshes[graphics.mesh].indexBuffer, 0 }, VK_INDEX_TYPE_UINT16);
                    mainCommandBuffer.cmdBindVertexBuffer({ meshes[graphics.mesh].vertexBuffer, 0 });
                }

                if(graphics.texture != lastTexture) {
                    const t2::DescriptorSet *set = &textures[graphics.texture].set.get();
                    mainCommandBuffer.cmdBindDescriptorSets(materials[graphics.material].pipeline, 0, { 1, &set });
                }

                uint32_t offset = i * uniformBufferSize;
                const t2::DescriptorSet *set = &uniformDescriptor.get();
                mainCommandBuffer.cmdBindDescriptorSets(materials[graphics.material].pipeline, 1, { 1, &set }, { 1, &offset });
                mainCommandBuffer.cmdDrawIndexed(meshes[graphics.mesh].indexBuffer.size / sizeof(uint16_t));
                i += 1;
            });
        }
        mainCommandBuffer.cmdEndRenderPass();
        mainCommandBuffer.end();
        
        engine.queues.graphics.submitCommandBuffer(mainCommandBuffer, sync);
        engine.queues.present.present(swapchain, nextImage, sync);
    }

    void init_() {
        DEBUG("initializing...");
        engine.initialize();
        allocator.initialize();
        swapchain.initialize();

        descriptorSetLayouts.emplace_back(engine); // texture
        descriptorSetLayouts.emplace_back(engine); // entity

        t2::DescriptorSetLayoutBinding setLayoutBindings[2][1] = {
            { { t2::DESCRIPTOR_COMBINED_IMAGE_SAMPLER, 0, 1, t2::SHADER_FRAGMENT_BIT }, },
            { { t2::DESCRIPTOR_UNIFORM_BUFFER_DYNAMIC, 0, 1, t2::SHADER_VERTEX_BIT }, }
        };

        t2::DescriptorPoolSizeForType poolSizes[] = {
            { t2::DESCRIPTOR_COMBINED_IMAGE_SAMPLER, 9 },
            { t2::DESCRIPTOR_UNIFORM_BUFFER_DYNAMIC, 1 },
        };

        
        descriptorSetLayouts[0].initalize({ 1, setLayoutBindings[0] });
        descriptorSetLayouts[1].initalize({ 1, setLayoutBindings[1] });
        descriptorPool.initialize(10, { 2, poolSizes });

        VkAttachmentDescription colorAttachment
        {
            .format = swapchain.surfaceFormat.format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        };

        VkAttachmentDescription depthAttachment
        {
            .format = VK_FORMAT_D32_SFLOAT,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        };

        t2::Subpass subpass;
        {
            subpass.bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

            subpass.dependency.enabled = false;
            subpass.dependency.subpass = VK_SUBPASS_EXTERNAL;
            subpass.dependency.dependencyFlags = 0;
            subpass.dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
            subpass.dependency.srcAccessMask = 0;
            subpass.dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
            subpass.dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

            subpass.references.push_back(t2::AttachmentReference {
                .type = t2::SUBPASS_ATTACHMENT_TYPE_COLOR,
                .attachment = 0,
                .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
            });

            subpass.references.push_back(t2::AttachmentReference {
                .type = t2::SUBPASS_ATTACHMENT_TYPE_DEPTH_STENCIL,
                .attachment = 1,
                .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
            });
        }
        
        depthImage = std::move(allocator.createImage(t2::ImageInfo {
            .type = t2::IMAGE_TYPE_2D,
            .extent = { .width = swapchain.extent.width, .height = swapchain.extent.width, .depth = 1 },
            .mipLevels = 1,
            .arrayLayers = 1,
            .format = VK_FORMAT_D32_SFLOAT,
            .tiling = t2::IMAGE_TILING_OPTIMAL,
            .isPreinit = false,
            .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .samples = VK_SAMPLE_COUNT_1_BIT,
        }));

        depthView.initialize(VK_IMAGE_VIEW_TYPE_2D, VK_FORMAT_D32_SFLOAT, {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        });

        renderPass.initialize({ colorAttachment, depthAttachment }, { subpass });
        swapchain.initializeFramebuffers(renderPass, &depthView);

        sync.initialize();
        commandPool.initialize(engine.physicalDevice.queueFamilies.graphics);
        mainCommandBuffer.initialize(commandPool);
        
        imageSampler.initialize(t2::ImageSamplerInfo{
            .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
            .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
            .filter = {
                .mag = VK_FILTER_LINEAR,
                .min = VK_FILTER_LINEAR,
            },
            .addressMode = {
                .u = VK_SAMPLER_ADDRESS_MODE_REPEAT,
                .v = VK_SAMPLER_ADDRESS_MODE_REPEAT,
                .w = VK_SAMPLER_ADDRESS_MODE_REPEAT,
            },
            .anisotropy = {
                .enable = false,
                .max = 1.0f
            },
            .compare = {
                .enable = false,
                .op = VK_COMPARE_OP_ALWAYS
            },
            .lod = {
                .bias = 0.0f,
                .min = 0.0f,
                .max = 0.0f
            }
        });

    }

    void deinit_() {
        DEBUG("deinitializing...");
        imageSampler.deinitialize();

        for(auto &texture : textures) {
            texture.view.deinitialize();
            allocator.destroyImage(texture.image);
        }

        for(auto &mesh : meshes) {
            allocator.destroyBuffer(mesh.vertexBuffer);
            allocator.destroyBuffer(mesh.indexBuffer);
        }

        for(auto &material : materials) {
            material.shader.deinitialize();
            material.pipeline.deinitialize();
        }

        depthView.deinitialize();
        allocator.destroyImage(depthImage);
        for(auto &layout : descriptorSetLayouts)
            layout.deinitialize();
        descriptorPool.deinitialize();
        commandPool.deinitialize();
        sync.deinitialize();
        renderPass.deinitialize();
        swapchain.deinitialize();
        allocator.deinitialize();
        engine.deinitialize();
    }

    int run(App *app) {
        if(!glfwInit()) {
            const char *errorString;
            glfwGetError(&errorString);
            DEBUG_ERROR("Failed to initialize GLFW. Error: " << errorString);
            return 1;
        }

        DEBUG("Initialized GLFW");
        try {
            init_();
            
            struct UniformBuffer {
                glm::mat4 transform;
            };

            auto minUboAlignment = engine.physicalDevice.properties.limits.minUniformBufferOffsetAlignment;
            uniformBufferSize = sizeof(UniformBuffer);
            if(minUboAlignment > 0)
                uniformBufferSize = (uniformBufferSize + minUboAlignment - 1) & ~(minUboAlignment - 1);
            DEBUG("minUboAlignment   = " << minUboAlignment);
            DEBUG("uniformBufferSize = " << uniformBufferSize);

            uniformDescriptor = descriptorPool.allocate(descriptorSetLayouts[1]);
#define MAX_QUADS 128
            uint8_t *uniformData = new uint8_t[uniformBufferSize * MAX_QUADS];

            auto uniformBuffer = allocator.createBuffer(
                uniformBufferSize * MAX_QUADS,
                VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                t2::ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT
            );

            uniformDescriptor.get().write(0, t2::DescriptorBufferInfo{
                .buffer = uniformBuffer,
                .offset = 0,
                .size = sizeof(UniformBuffer)
            });

            app->initialize();

            DEBUG("starting...");

            size_t frameCounter = 0;
            float totalTime = 0;
            float lastTime = engine.getTime();
            while(!engine.shouldEnd()) {
                float currentTime = engine.getTime();
                float deltaTime = currentTime - lastTime;
                totalTime += deltaTime;
                frameCounter += 1;

                sync.waitFrame();

                if(mainCamera != entt::tombstone) {
                    auto projectionMatrix = registry.get<CameraComponent>(mainCamera).getProjection(game, mainCamera);
                    auto cameraTransform = registry.get_or_emplace<TransformComponent>(
                        mainCamera,
                        glm::vec3 { 0.0f, 0.0f, 0.0f },
                        glm::identity<glm::quat>(),
                        glm::vec3 { 1.0f, 1.0f, 1.0f }
                    );

                    auto viewMatrix = /*glm::inverse*/(cameraTransform.getMatrix());

                    app->update(deltaTime);

                    size_t renderableCount = 0;
                    glm::ivec2 framebufferSize;
                    glfwGetFramebufferSize(engine.window, &framebufferSize.x, &framebufferSize.y);

                    registry.view<TransformComponent, GraphicsComponent>().each([&](auto &transform, auto &graphics) {
                        uint32_t offset = renderableCount * uniformBufferSize;
                        if(offset >= uniformBufferSize * MAX_QUADS) return;
                        UniformBuffer *buffer = (UniformBuffer*)(uniformData + offset);

                        buffer->transform = projectionMatrix * viewMatrix * transform.getMatrix();

                        renderableCount += 1;
                    });

                    uniformBuffer.write<uint8_t>({ uniformBufferSize * (renderableCount), uniformData });
                    
                    frame_();
                } else {
                    app->update(deltaTime);
                }

                engine.frame();

                if(totalTime >= 0.1f) {
                    float fps = frameCounter / totalTime;
                    engine.setTitle("FPS: " + std::to_string((int)fps));
                    totalTime = 0.0f;
                    frameCounter = 0;
                }

                lastTime = currentTime;
            }

            vkDeviceWaitIdle(engine.device);

            app->deinitialize();
            delete[] uniformData;
            allocator.destroyBuffer(uniformBuffer);
            deinit_();
        } catch(std::runtime_error &e) {
            DEBUG_ERROR(e.what());
            return 1;
        }

        DEBUG("Done");
        glfwTerminate();
        return 0;
    }
};

Game::Game() {
    impl = new Impl(
        *this,
        t2::AppInfo {
            .name = "game",
            .version = VK_MAKE_VERSION(0, 1, 0),
            .width = 800,
            .height = 600,
        }
    );
}

glm::mat4 CameraComponent::getProjection(Game &game, entt::entity) const {
    auto framebufferSize = game.getFramebufferSize();
    float aspect = framebufferSize.x /(float) framebufferSize.y;
    switch(type) {
    case CameraType::ePixel:
        return glm::ortho(
            pixel.offset.x * pixel.scale,
            (pixel.offset.x + framebufferSize.x) * pixel.scale,
            pixel.offset.y * pixel.scale,
            (pixel.offset.y + framebufferSize.y) * pixel.scale
        );
    case CameraType::ePerspective:
        return glm::perspective(
            perspective.fov,
            aspect,
            perspective.near,
            perspective.far
        );
    case CameraType::eOrthographic:
        return glm::ortho(
            -orthographic.size / 2.0f,
            +orthographic.size / 2.0f,
            -orthographic.size / aspect / 2.0f,
            +orthographic.size / aspect / 2.0f
        );
    }
}

glm::mat4 TransformComponent::getMatrix() const {
    auto t = glm::mat4(1.0f);
    t = glm::translate(t, position);
    t = t * glm::mat4_cast(rotation);
    t = glm::scale(t, scale);
    return t;
}

size_t Game::createMesh(u::span<Vertex> vertices, u::span<uint16_t> indices) {
    return impl->createMesh(vertices, indices);
}

size_t Game::createTexture(const char *path) {
    return impl->createTexture(path);
}

size_t Game::createMaterial(const char *vertexPath, const char *fragmentPath) {
    return impl->createMaterial(vertexPath, fragmentPath);
}

void Game::setMainCamera(entt::entity cameraEntity) {
    impl->mainCamera = cameraEntity;
}

bool Game::isKeyDown(Key key) const {
    return glfwGetKey(impl->engine.window, (int)key);
}

bool Game::isMouseDown(int button) const {
    return glfwGetMouseButton(impl->engine.window, button);
}

static bool getMouseMonitor(
    GLFWmonitor *&monitor,
    GLFWwindow *window
) {
    bool success = false;

    double cursor_position[2] = {0};
    glfwGetCursorPos(window, &cursor_position[0], &cursor_position[1]);

    int window_position[2] = {0};
    glfwGetWindowPos(window, &window_position[0], &window_position[1]);

    int monitors_size = 0;
    GLFWmonitor **monitors = glfwGetMonitors(&monitors_size);

    // convert cursor position from window coordinates to screen coordinates
    cursor_position[0] += window_position[0];
    cursor_position[1] += window_position[1];

    for(int i = 0; ((!success) && (i < monitors_size)); ++i)
    {
        int monitor_position[2] = {0};
        glfwGetMonitorPos(monitors[i], &monitor_position[0], &monitor_position[1]);

        const GLFWvidmode *monitor_video_mode = glfwGetVideoMode(monitors[i]);

        if(
            (cursor_position[0] < monitor_position[0]) ||
            (cursor_position[0] > (monitor_position[0] + monitor_video_mode->width)) ||
            (cursor_position[1] < monitor_position[1]) ||
            (cursor_position[1] > (monitor_position[1] + monitor_video_mode->height))
        ) {
            monitor = monitors[i];
            success = true;
        }
    }

    // true: monitor contains the monitor the mouse is on
    // false: monitor is unmodified
    return success;
}

glm::vec2 Game::getMousePosition() const {
    double xpos, ypos;
    glfwGetCursorPos(impl->engine.window, &xpos, &ypos);
    if(GLFWmonitor *monitor; getMouseMonitor(monitor, impl->engine.window)) {
        float sx, sy;
        glfwGetMonitorContentScale(monitor, &sx, &sy);
        xpos *= sx;
        ypos *= sy;
    }
    return { xpos, ypos };
}

entt::registry &Game::getRegistry() const {
    return impl->registry;
}

glm::uvec2 Game::getFramebufferSize() const {
    auto size = impl->engine.getFramebufferSize();
    return { size.width, size.height };
}

int Game::run(App *app) {
    return impl->run(app);
}

Game::~Game() {
    delete impl;
}
