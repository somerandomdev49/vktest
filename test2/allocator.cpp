#include "allocator.hpp"
#include <algorithm>
#include <vk_mem_alloc.h>
#include "engine.hpp"

namespace t2 {
	void Allocator::initialize() {
		VmaAllocatorCreateInfo createInfo {};
		createInfo.physicalDevice = this->engine.physicalDevice.device;
		createInfo.instance = this->engine.instance;
		createInfo.device = this->engine.device;
		createInfo.vulkanApiVersion = VK_API_VERSION_1_0;
		
		CHECKVK(
			vmaCreateAllocator(&createInfo, (VmaAllocator*)&this->allocator),
			"Failed to create memory allocator"
		);
	}

	void Allocator::deinitialize() {
		vmaDestroyAllocator((VmaAllocator)this->allocator);
	}

	Buffer Allocator::createBuffer(size_t size, VkBufferUsageFlags bufferUsage, VkFlags flags) {
		VkBufferCreateInfo bufferInfo { };
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = bufferUsage;
		
		VmaAllocationCreateInfo allocInfo {};
		allocInfo.usage = VMA_MEMORY_USAGE_AUTO;
		allocInfo.flags = (VmaAllocationCreateFlags)flags;

		Buffer buffer(*this, size);
		CHECKVK(
			vmaCreateBuffer(
				(VmaAllocator)this->allocator,
				&bufferInfo,
				&allocInfo,
				&buffer.buffer,
				(VmaAllocation*)&buffer.allocation,
				nullptr
			),
			"Failed to allocate memory on the GPU."
		);

		return buffer;
	}

	void Allocator::destroyBuffer(Buffer &buffer) {
		vmaDestroyBuffer((VmaAllocator)this->allocator, buffer.buffer, (VmaAllocation)buffer.allocation);
	}

	Image Allocator::createImage(const ImageInfo &info, VkFlags flags) {
		VkImageCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		createInfo.imageType = (VkImageType)info.type;
		createInfo.extent = info.extent;
		createInfo.mipLevels = info.mipLevels;
		createInfo.arrayLayers = info.arrayLayers;
		createInfo.format = info.format;
		createInfo.tiling = (VkImageTiling)info.tiling;
		createInfo.initialLayout = info.isPreinit ?
			VK_IMAGE_LAYOUT_PREINITIALIZED : VK_IMAGE_LAYOUT_UNDEFINED;
		createInfo.usage = (VkImageUsageFlags)info.usage;
		createInfo.sharingMode = info.sharingMode;
		createInfo.samples = (VkSampleCountFlagBits)info.samples;

		VmaAllocationCreateInfo allocInfo {};
		allocInfo.usage = VMA_MEMORY_USAGE_AUTO;
		allocInfo.flags = (VmaAllocationCreateFlags)flags;

		Image image(*this, info.extent, createInfo.initialLayout);
		// image.image = nullptr;
		// image.allocation = nullptr;
		CHECKVK(
			vmaCreateImage(
				(VmaAllocator)this->allocator,
				&createInfo,
				&allocInfo,
				&image.image,
				(VmaAllocation*)&image.allocation,
				nullptr
			),
			"Failed to allocate image memory on the GPU."
		);

		DEBUG("created image: " << image.image);
		return std::move(image);
	}

	void Allocator::destroyImage(Image &image) {
		vmaDestroyImage((VmaAllocator)this->allocator, image.image, (VmaAllocation)image.allocation);
	}

	void Allocator::writeBuffer(Buffer &buffer, u::span<const uint8_t> data) {
		CHECK(data.count <= buffer.size, "Tried to write more than allocated!");
		void *bufferData;
		CHECKVK_NOLOG(vmaMapMemory((VmaAllocator)this->allocator, (VmaAllocation)buffer.allocation, &bufferData),
		              "Failed to map GPU buffer memory");
		std::copy(data.values, data.values + data.count, (uint8_t*)bufferData);
		vmaUnmapMemory((VmaAllocator)this->allocator, (VmaAllocation)buffer.allocation);
		CHECKVK_NOLOG(vmaFlushAllocation((VmaAllocator)this->allocator, (VmaAllocation)buffer.allocation, 0, data.count),
		              "Failed to flush allocation cache after write");
	}

	Image &Image::operator=(Image &&other) {
		this->allocator = other.allocator;
		this->allocation = other.allocation;
		this->extent = other.extent;
		this->layout = other.layout;
		this->image = other.image;
		DEBUG("this->image = other.image: " << other.image);
		return *this;
	}
}
