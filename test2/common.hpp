#pragma once
#define GLM_FORCE_LEFT_HANDED
#define GLM_DEPTH_ZERO_TO_ONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "span.hpp"

#include <string_view>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <limits>
#include <array>
#include <stack>
#include <set>

extern int DEBUG__depth;

#define CHECKEQ_FAIL_(MSG,X,FILE,LINE) throw std::runtime_error(std::string(MSG)+" ("+std::to_string(x)+") @ "+FILE+':'+std::to_string(LINE))
#define CHECKEQ_(EXPR, EQ, MSG, FILE, LINE) {const auto&x=(EXPR);if(x!=(EQ))CHECKEQ_FAIL_(MSG,x,FILE,LINE);}
#define CHECKEQ(EXPR, EQ, MSG) CHECKEQ_(EXPR, EQ, MSG, __FILE__, __LINE__)
#define CHECK(EXPR, MSG) CHECKEQ_(EXPR, true, MSG, __FILE__, __LINE__)
#define CHECKVK_NOLOG_(EXPR, MSG, FILE, LINE) CHECKEQ_((EXPR), VK_SUCCESS, (MSG), FILE, LINE)
#define CHECKVK_NOLOG(EXPR, MSG) CHECKVK_NOLOG_((EXPR), (MSG), __FILE__, __LINE__)
#define CHECKVK_LOG_(FILE, LINE, EXPRSTR) DEBUG_GENERIC("CHECK", EXPRSTR << " @ " << FILE << ':' << LINE, "\033[0;34m")
#define CHECKVK(EXPR, MSG) { CHECKVK_LOG_(__FILE__, __LINE__, #EXPR); CHECKVK_NOLOG_((EXPR), (MSG), __FILE__, __LINE__); }
#define DEBUG_INDENT_SPACES() std::string(DEBUG__depth, ' ')
#define DEBUG_INDENT() DEBUG_INDENT_SPACES() << DEBUG_INDENT_SPACES()
#define DEBUG_GENERIC(LBL, MSG, COLOR) std::cout << LBL << " | " COLOR << DEBUG_INDENT() << MSG << "\033[0;0m" << std::endl
#define DEBUG(MSG) DEBUG_GENERIC("DEBUG", MSG, "\033[0;32m")
#define DEBUG_START(MSG) { DEBUG(MSG); DEBUG__depth += 1; }
#define DEBUG_END(MSG) { DEBUG__depth -= 1; DEBUG(MSG); }
#define DEBUG_END_NOMSG() { DEBUG__depth -= 1; }
#define DEBUG_ERROR(MSG) DEBUG_GENERIC("\033[0;31mERROR\033[0;0m", MSG, "\033[0;31m")

/**
 * @brief Logs the start and the end of a lexical scope.
 * 
 */
class ScopedDebug {
public:
    ScopedDebug(const char *scopename) : name(scopename) {
        DEBUG_START(name << "() started!");
    }

    ~ScopedDebug() {
        DEBUG_END(name << "() ended!");
    }

private:
    const char *name;
};

#define DEBUG_FUNC() ScopedDebug DEBUG__scoped__(__func__);
#define DEBUG_FUNC_SHORT() DEBUG(__func__ << "()");
#define OPTIONAL_DATA(CONTAINER) ((CONTAINER).size() != 0 ? (CONTAINER).data() : nullptr)

/**
 * @brief Useful class that provides a way to defer object deinitialization.
 * 
 * @tparam T deinit function type.
 */
template<typename T>
class DeInitable {
	std::stack<std::function<T>> deinitFuncs;
protected:
	inline void constructDeInitable(DeInitable<T> &other) {
		std::swap(deinitFuncs, other.deinitFuncs);
	}

	inline size_t deinit(const std::function<T> &f) {
		deinitFuncs.push(f);
		return deinitFuncs.size();
	}
public:
	void deinitialize() {
		while(!deinitFuncs.empty()) {
			deinitFuncs.top()();
			deinitFuncs.pop();
		}
	}
};

namespace t2 {
	using Handle = void*;
	struct Engine;                // fwd-decl
	struct SyncObjects;           // fwd-decl
	struct CommandBuffer;         // fwd-decl
	struct AppInfo;               // fwd-decl
	struct Queue;                 // fwd-decl
	struct QueueFamilies;         // fwd-decl
	struct SurfaceSupportDetails; // fwd-decl
	struct PhysicalDevice;        // fwd-decl
	struct Framebuffer;           // fwd-decl
	struct Pipeline;              // fwd-decl
	struct AttachmentReference;   // fwd-decl
	struct Dependency;            // fwd-decl
	struct Subpass;               // fwd-decl
	struct RenderPass;            // fwd-decl
	struct ShaderModule;          // fwd-decl
	struct Shader;                // fwd-decl
	struct Swapchain;             // fwd-decl
	struct Allocator;             // fwd-decl
	struct DescriptorSetLayout;   // fwd-decl
	struct DescriptorPool;        // fwd-decl
	struct DescriptorSet;         // fwd-decl
}
