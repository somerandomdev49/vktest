#include "common.hpp"
#include "engine.hpp"
#include "descriptors.hpp"

namespace t2 {
    void DescriptorSetLayout::initalize(u::span<const DescriptorSetLayoutBinding> bindings) {
        this->bindings.reserve(bindings.size());
        for(const auto &binding : bindings) this->bindings.push_back(binding);
        std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
        for(const auto &binding : bindings) {
            VkDescriptorSetLayoutBinding layoutBinding{};
            layoutBinding.binding = binding.binding;
            layoutBinding.descriptorType = (VkDescriptorType)binding.type;
            layoutBinding.descriptorCount = binding.count;
            layoutBinding.stageFlags = binding.usedIn;
            layoutBinding.pImmutableSamplers = nullptr; /* TODO */
            layoutBindings.push_back(layoutBinding);
        }

        VkDescriptorSetLayoutCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        createInfo.bindingCount = layoutBindings.size();
        createInfo.pBindings = layoutBindings.data();

        CHECKVK(
            vkCreateDescriptorSetLayout(this->engine.device, &createInfo, nullptr, &this->layout),
            "Could not create descriptor set layout"
        );

        deinit([this]() {
            vkDestroyDescriptorSetLayout(this->engine.device, this->layout, nullptr);
        });
    }

    void DescriptorSet::write(uint32_t binding, const DescriptorImageInfo &descriptorImageInfo) {
        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = descriptorImageInfo.layout;
        imageInfo.imageView = descriptorImageInfo.view.view;
        imageInfo.sampler = descriptorImageInfo.sampler.sampler;

        bool found = false;
        VkWriteDescriptorSet descriptorWrite{};
        for(const auto &bind : this->layout.bindings) {
            if(bind.binding != binding) continue;
            descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrite.dstBinding = bind.binding;
            descriptorWrite.descriptorType = (VkDescriptorType)bind.type;
            descriptorWrite.dstSet = this->set;
            descriptorWrite.descriptorCount = 1;
            descriptorWrite.dstArrayElement = 0;
            descriptorWrite.pImageInfo = &imageInfo;
            found = true;
        }

        CHECK(found, "Could not find (image) descriptor with specified binding. #" + std::to_string(binding));
        if(found)
            vkUpdateDescriptorSets(this->pool.engine.device, 1, &descriptorWrite, 0, nullptr);
    }

    void DescriptorSet::write(uint32_t binding, const DescriptorBufferInfo &descriptorBufferInfo) {
        VkDescriptorBufferInfo bufferInfo{};
        bufferInfo.buffer = descriptorBufferInfo.buffer.buffer;
        bufferInfo.offset = descriptorBufferInfo.offset;
        bufferInfo.range = descriptorBufferInfo.size;

        bool found = false;
        VkWriteDescriptorSet descriptorWrite{};
        for(const auto &bind : this->layout.bindings) {
            if(bind.binding != binding) continue;
            descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrite.dstBinding = bind.binding;
            descriptorWrite.descriptorType = (VkDescriptorType)bind.type;
            descriptorWrite.dstSet = this->set;
            descriptorWrite.descriptorCount = 1;
            descriptorWrite.dstArrayElement = 0;
            descriptorWrite.pBufferInfo = &bufferInfo;
            found = true;
        }

        CHECK(found, "Could not find (buffer) descriptor with specified binding.");
        if(found)
            vkUpdateDescriptorSets(this->pool.engine.device, 1, &descriptorWrite, 0, nullptr);
    }

    void DescriptorPool::initialize(uint32_t setCount, u::span<const DescriptorPoolSizeForType> types) {
        // VkDescriptorPoolSize == DescriptorPoolSizeForType
        VkDescriptorPoolCreateInfo poolInfo{};
        poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        poolInfo.poolSizeCount = types.size();
        poolInfo.pPoolSizes = (VkDescriptorPoolSize*)types.data();
        poolInfo.maxSets = setCount;

        CHECKVK(
            vkCreateDescriptorPool(this->engine.device, &poolInfo, nullptr, &this->pool),
            "Could not create descriptor pool."
        )
    }

    void DescriptorPool::deinitialize() {
        vkDestroyDescriptorPool(this->engine.device, this->pool, nullptr);
    }

    DescriptorSet DescriptorPool::allocate(const DescriptorSetLayout &layout) {
        VkDescriptorSetAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = this->pool;
        allocInfo.descriptorSetCount = 1;
        allocInfo.pSetLayouts = &layout.layout;
        VkDescriptorSet set;

        CHECKVK(
            vkAllocateDescriptorSets(this->engine.device, &allocInfo, &set),
            "Failed to allocate descriptor sets"
        );

        return DescriptorSet{ *this, layout, set };
    }
}
