#include "game.hpp"
#include "../test2/platform.hpp"

int main(int argc, char **argv) {
    platform_prep(); // sets useful stuff for each platform (e.g. allows ANSI escapes on Windows)

    const char *appName = "App2D";
    if(argc > 1) appName = argv[1];

    Game game;
    
    if(game.getAppRegistry().find(appName) == game.getAppRegistry().end()) {
        DEBUG_ERROR("couldn't find app '" << appName << "'!");
        return 1;
    }

    auto app = game.getAppRegistry()[appName](game);
    int ret = game.run(app);
    delete app;
    return ret;
}
