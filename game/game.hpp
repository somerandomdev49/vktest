#pragma once
#include <test2/common.hpp>
#include <test2/vertex.hpp>
#include <stdexcept>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <cstdint>
#include <entt/entity/registry.hpp>
#include <unordered_map>
#include <string>

struct Vertex {
	using Info = t2::VertexBase<glm::vec3, glm::vec3, glm::vec2>;
	glm::vec3 position;
	glm::vec3 color;
	glm::vec2 texcoord;
};


enum class Key {
    eUnknown = -1,
    eSpace = 32,
    eApostrophe = 39 /* ' */,
    eComma = 44 /* , */,
    eMinus = 45 /* - */,
    ePeriod = 46 /* . */,
    eSlash = 47 /* / */,
    e0 = 48,
    e1 = 49,
    e2 = 50,
    e3 = 51,
    e4 = 52,
    e5 = 53,
    e6 = 54,
    e7 = 55,
    e8 = 56,
    e9 = 57,
    eSemicolon = 59 /* ; */,
    eEqual = 61 /* = */,
    eA = 65,
    eB = 66,
    eC = 67,
    eD = 68,
    eE = 69,
    eF = 70,
    eG = 71,
    eH = 72,
    eI = 73,
    eJ = 74,
    eK = 75,
    eL = 76,
    eM = 77,
    eN = 78,
    eO = 79,
    eP = 80,
    eQ = 81,
    eR = 82,
    eS = 83,
    eT = 84,
    eU = 85,
    eV = 86,
    eW = 87,
    eX = 88,
    eY = 89,
    eZ = 90,
    eLeftBracket = 91 /* [ */,
    eBackslash = 92 /* \ */,
    eRightBracket = 93 /* ] */,
    eGraveAccent = 96 /* ` */,
    eWorld1 = 161 /* non-US #1 */,
    eWorld2 = 162 /* non-US #2 */,
    eEscape = 256,
    eEnter = 257,
    eTab = 258,
    eBackspace = 259,
    eInsert = 260,
    eDelete = 261,
    eRight = 262,
    eLeft = 263,
    eDown = 264,
    eUp = 265,
    ePage_up = 266,
    ePage_down = 267,
    eHome = 268,
    eEnd = 269,
    eCapsLock = 280,
    eScrollLock = 281,
    eNumLock = 282,
    ePrintScreen = 283,
    ePause = 284,
    eF1 = 290,
    eF2 = 291,
    eF3 = 292,
    eF4 = 293,
    eF5 = 294,
    eF6 = 295,
    eF7 = 296,
    eF8 = 297,
    eF9 = 298,
    eF10 = 299,
    eF11 = 300,
    eF12 = 301,
    eF13 = 302,
    eF14 = 303,
    eF15 = 304,
    eF16 = 305,
    eF17 = 306,
    eF18 = 307,
    eF19 = 308,
    eF20 = 309,
    eF21 = 310,
    eF22 = 311,
    eF23 = 312,
    eF24 = 313,
    eF25 = 314,
    eKp_0 = 320,
    eKp_1 = 321,
    eKp_2 = 322,
    eKp_3 = 323,
    eKp_4 = 324,
    eKp_5 = 325,
    eKp_6 = 326,
    eKp_7 = 327,
    eKp_8 = 328,
    eKp_9 = 329,
    eKpDecimal = 330,
    eKpDivide = 331,
    eKpMultiply = 332,
    eKpSubtract = 333,
    eKpAdd = 334,
    eKpEnter = 335,
    eKpEqual = 336,
    eLeftShift = 340,
    eLeftControl = 341,
    eLeftAlt = 342,
    eLeftSuper = 343,
    eRightShift = 344,
    eRightControl = 345,
    eRightAlt = 346,
    eRightSuper = 347,
    eMenu = 348,
};

class Game;

class App {
protected:
    Game &game;
public:
    App(Game &game) : game(game) {}
    virtual ~App() = default;
    virtual void initialize() = 0;
    virtual void update(float deltaTime) = 0;
    virtual void deinitialize() = 0;
};

class Game {
    struct Impl;
    Impl *impl;
    friend class CameraComponent;
public:
    Game();
	size_t createMesh(u::span<Vertex> vertices, u::span<uint16_t> indices);
	size_t createTexture(const char *path);
	size_t createMaterial(const char *vertexPath, const char *fragmentPath);

    bool isKeyDown(Key key) const;
    bool isMouseDown(int button) const;
    glm::vec2 getMousePosition() const;
    entt::registry &getRegistry() const;
    glm::uvec2 getFramebufferSize() const;
    void setMainCamera(entt::entity cameraEntity);

    entt::entity createEntity() { return getRegistry().create(); }

	int run(App *app);
    ~Game();

    using AppRegistry = std::unordered_map<std::string, App*(*)(Game&)>;
    static AppRegistry &getAppRegistry() {
        static AppRegistry registry;
        return registry;
    }
};

struct TransformComponent {
	glm::vec3 position;
	glm::quat rotation;
	glm::vec3 scale;

    glm::mat4 getMatrix() const;

    static TransformComponent identity() {
        return {
            { 0.0f, 0.0f, 0.0f },
            glm::identity<glm::quat>(),
            { 1.0f, 1.0f, 1.0f },
        };
    }
};

struct GraphicsComponent {
	size_t material;
    size_t texture;
    size_t mesh;
};

struct CameraComponent {
    enum class CameraType {
        ePerspective,
        eOrthographic,
        ePixel
    } type;

    union {
        struct {
            float fov;
            float far, near;
        } perspective;
        struct {
            float size;
        } orthographic;
        struct {
            float scale;
            glm::vec2 offset;
        } pixel;
    };

    CameraComponent(CameraType type) : type(type) {}

private:
    friend class Game::Impl;
    glm::mat4 getProjection(Game &game, entt::entity) const;
};

namespace detail_ {
    template<typename T>
    class AppRegister;
}

#define REGISTER_APP(CLASS, NAME) \
    namespace detail_ { \
        template<> \
        struct AppRegister<CLASS> { \
            static AppRegister<CLASS> reg; \
            AppRegister() { \
                Game::getAppRegistry()[NAME] = [](Game &game) -> App* { \
                    return new CLASS(game); \
                }; \
            } \
        }; \
        AppRegister<CLASS> AppRegister<CLASS>::reg = {}; \
    }
